<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_to_Lead_Email</fullName>
        <description>Email to Lead Email</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Lead_Followup_needed</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update</fullName>
        <field>Rating</field>
        <literalValue>Hot</literalValue>
        <name>Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>TestMessage</fullName>
        <apiVersion>40.0</apiVersion>
        <endpointUrl>https://putsreq.com/eYadBrSfQlfCknUpFcCH</endpointUrl>
        <fields>Id</fields>
        <fields>LeadSource</fields>
        <fields>Status</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>saurabhkuma@sales.com</integrationUser>
        <name>TestMessage</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Sent Email when condition met</fullName>
        <actions>
            <name>Email_to_Lead_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TestMessage</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Open - Not Contacted</value>
        </criteriaItems>
        <description>when  Lead Source	= Web and  Lead Status = Open - Not Contacted

1. Update  Rating	 = Hot
2. Send an email to  field :Email</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
