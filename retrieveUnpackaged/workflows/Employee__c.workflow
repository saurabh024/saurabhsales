<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>update_Employee_attendacne</fullName>
        <field>Employee_Attendance__c</field>
        <formula>Name&amp;TEXT(Date__c)</formula>
        <name>update Employee attendacne</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Employee Unique attendance</fullName>
        <actions>
            <name>update_Employee_attendacne</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR( ISCHANGED(Date__c), ISCHANGED( Name ) , ISNEW() )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
