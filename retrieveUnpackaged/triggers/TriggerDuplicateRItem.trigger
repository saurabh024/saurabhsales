trigger TriggerDuplicateRItem on DuplicateRecordItem (after insert ,after update) {

 //update owner Name of duplicate to duplicate Owner
 Map<String,String> keys = new Map<String,String>();
 Map<String,Schema.SobjectType> describe = Schema.getGlobalDescribe();
 for(String s:describe.keyset())
 keys.put(describe.get(s).getDescribe().getKeyPrefix(),s);
 List<Concierge__c> listDubConciergetoUpdated = new List<Concierge__c>();

 for(DuplicateRecordItem dr : Trigger.New){
   if(keys.get(String.valueOf(dr.RecordId).subString(0,3)) == 'Concierge__c'){
        listDubConciergetoUpdated.add(new Concierge__c(Id = dr.RecordId , Owner_Name__c ='Duplicate Owner'));
   }
 }
 
 if(!listDubConciergetoUpdated.isEmpty()){
  update listDubConciergetoUpdated;
 }
}