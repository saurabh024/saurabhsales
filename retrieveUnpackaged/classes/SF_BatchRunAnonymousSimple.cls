public class SF_BatchRunAnonymousSimple  implements Database.Batchable<String> , Database.AllowsCallouts, Database.stateful {
   
    public  integer noofIteration;
    public String anonymousname ;
     public List<String> logginDetail;
    public SF_BatchRunAnonymousSimple( String anonymousname ,integer noofIteration){
        this.noofIteration = noofIteration;
        this.anonymousname = anonymousname;
        logginDetail = new ExecuteAnonymousCode().getSessionID();
    }
   
    public Iterable<String> start(Database.BatchableContext BC) {
        
        List<String>  liststring = new List<String>();
            For(Integer i = 0; i < noofIteration ; i++){
                liststring.add( String.valueOf(i));
            }
       // System.assertEquals(liststring, null);
        return liststring;
        
    }

    public void execute(Database.BatchableContext info, List<String> strings) {
       
        
        AutomateSolution__c ac = [Select Id ,Anonymous_Code__c from AutomateSolution__c where Name = :anonymousname];   
        String stringtoExecute = ac.Anonymous_Code__c;
                   
        System.debug('*** Exceuted lines ***\n'+stringtoExecute);

         
          ExecuteAnonymousCode cd = new ExecuteAnonymousCode();          
        
          cd.executeCodewithLoginDetail(stringtoExecute ,logginDetail);
        

    }

    public void finish(Database.BatchableContext info) {}
}