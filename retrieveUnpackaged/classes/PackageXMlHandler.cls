//class  for making package.xml  for changed/added components 
public  class PackageXMlHandler{



   public Map<String ,List<String>> MapNameWithMembers{get; set{

        if(MapNameWithMembers ==Null){
        MapNameWithMembers = new Map<String ,  List<String>>();
        }

        }}
    public String  SIRName {get; set;}
    public list<String>  PakageXMLString {get; set;}
    public set<String>   UniqueMemberSet;
    public  String  String_PackageXMl {get; set;}
    public List<String> SIRList {get; set;}
    
    //variable used in package.xml
    public final static String PACKAGE_STRING = 'Package';
    public final static String PACKAGE_PREFIX = 'ih';
    public final static String DOMFULLNAME = 'fullName';
    public final static String INITIAL_TEXT = 'Migration test';
    public final static String DOMTYPES = 'types';
    public final static String MEMBERS = 'members';
    public final static String NAME = 'name';
    public final static String VERSION = 'version';
    public final static String VERSION_NO = '34.0';
    public final static String PACKAGEFILENAMESPACE = 'http://soap.sforce.com/2006/04/metadata';
   //Set to hold the components strings for whom ObjectName. Prefix is required for migration using Package.xml file
    public static Set<String> SET_COMPONENTS_NAME_HAVING_OBJECT_NAME_PREFIX_AS_MEMBER {

        get {

            //Create a Set of fields
            Set<String> setFields = new Set<String>();

            //Populating set with the value
            setFields.add('CustomField');
            setFields.add('CustomObjectOwnerSharingRule');
            setFields.add('CustomObjectCriteriaBasedSharingRule');
            setFields.add('RecordType');
            setFields.add('WorkflowRule');
            setFields.add('WorkflowFieldUpdate');
            setFields.add('WorkflowTask');
            setFields.add('WorkflowAlert');
            setFields.add('WebLink');
            setFields.add('ValidationRule');
            setFields.add('AssignmentRule');
            setFields.add('AutoResponseRule');
            setFields.add('BusinessProcess');
            setFields.add('EscalationRule');
            setFields.add('ListView');
            setFields.add('FieldSet');
            setFields.add('WorkflowOutboundMessage');
            setFields.add('ApprovalProcess');

            //Return Set
            return setFields;
        }
    }     
        
 // method for loggin into IH Sf org and retrieve session id 
 public  list<String> getSessionID(){
        
        partnerSoapSforceCom.LoginResult loginResult;       
        partnerSoapSforceCom.Soap sp= new partnerSoapSforceCom.Soap();
        List<String> loginInfo = new list<string>();

        String username = UtilityClass.Username;
        String password = UtilityClass.Password;
        loginResult = sp.login(username, password);
        system.debug('loginResult ' + loginResult);
        loginInfo.add(loginResult.serverUrl);
        loginInfo.add(loginResult.sessionId);
        return  loginInfo;
        
 }      
 
 public pageReference  createXML(){
 
 PageReference pre = new PageReference('/apex/SIR_PackageXML');
 
 return pre;
 }
 
 public void retrieveModification()
 {       
        Map<String , Listing_Component_Map__c> MapIhToAntObj = new Map<String ,Listing_Component_Map__c>();
        List<String> logginDetail = getSessionID();
        partnerSoapSforceCom.Soap sp= new partnerSoapSforceCom.Soap();
        sp.endpoint_x= logginDetail[0];
        partnerSoapSforceCom.SessionHeader_element header= new    partnerSoapSforceCom.SessionHeader_element();
        header.sessionId=logginDetail[1];
        sp.SessionHeader= header;
        if(SIRList == NULL){
         SIRList =  new List<String>();
         }
        SIRList.add(''+SIRName+'');
        string myQuery = 'select id , name ,Type_Of_Modification__c,Component_Type__c,Object_Name__c ,Component_Label__c,Component_Name__c  from Modification__c where  System_Issue_Report__r.name = \''+SIRName +'\' ';
        system.debug(myQuery);
        partnerSoapSforceCom.QueryResult  ModificationResult = sp.query(myQuery);
        sobjectPartnerSoapSforceCom.sObject_x[] ModificationRecord = ModificationResult.records;
        System.debug(ModificationRecord);
        if( ModificationRecord !=null && !ModificationRecord.isEmpty()){
         
              MapIhToAntObj  = Listing_Component_Map__c.getAll();
              if(MapNameWithMembers == Null){
                  MapNameWithMembers = new Map<String ,list<String>>();
                 }
               if(UniqueMemberSet == Null){
               UniqueMemberSet = new Set<String>();
               }  
              
               for(sobjectPartnerSoapSforceCom.sObject_x sob : ModificationRecord){
          
                          String antComponent = '' ;
                          String antMember    = '' ;
                     
                          if(MapIhToAntObj.containsKey(sob.ComponentType)){
                         
                          antComponent = antComponent+ MapIhToAntObj.get(sob.ComponentType).Ant_Component__c+'' ;
                           }
                           else{
                                  continue;
                            }
                      
                          if(antComponent != null && !UniqueMemberSet.contains(antComponent+':-'+sob.ObjectName+'.'+sob.ComponentAPIName+'')){
                            
                                    //component which required sobjec_c.member name 
                                     if(SET_COMPONENTS_NAME_HAVING_OBJECT_NAME_PREFIX_AS_MEMBER.contains(MapIhToAntObj.get(sob.ComponentType).Ant_Component__c)){
                                      
                                      antMember    = ''+sob.ObjectName+'.'+sob.ComponentAPIName+'';
                                     
                                      }
                                  
                                     else if(MapIhToAntObj.get(sob.ComponentType).Ant_Component__c.equalsIgnoreCase('Layout')){
                                      antMember    = ''+sob.ObjectName+'-'+sob.ComponentAPIName+'';
                                      }
                                    else{
                                    
                                      antMember    = ''+sob.ComponentAPIName+'';
                                       }
                                    
                                    if(MapNameWithMembers !=null && MapNameWithMembers.containsKey(antComponent)){
                                    
                                     MapNameWithMembers.get(antComponent).add(antMember);
                                     
                                    }else{
                                   
                                      MapNameWithMembers.put(antComponent , new list<String>{''+antMember});
                                    }
                      
                                UniqueMemberSet.add(antComponent+':-'+sob.ObjectName+'.'+sob.ComponentAPIName+'');
                 
                          }
                        
                   }
                   
                   
                   //making xml using document.dom
                   
                   //Dom Document
                   DOM.Document doc = new DOM.Document();
                   //Root element (Package)        
                   dom.XmlNode mypackage = doc.createRootElement(PACKAGE_STRING , PACKAGEFILENAMESPACE , null); 
                   //fullName Element
                   dom.XmlNode fullName = mypackage.addChildElement(DOMFULLNAME , null, null).addTextNode(INITIAL_TEXT);
        
                   PakageXMLString = new list<string>();
                   //code to make string for all modification 
                   for(String component : MapNameWithMembers.keySet()){
                   
                        //types element
                             dom.XmlNode types = mypackage.addChildElement(DOMTYPES , null, null);
                             
                             PakageXMLString.add( '&lt;types&gt; \n');
                             
                             for(String Member : MapNameWithMembers.get(component)){
                         
                                  types.addChildElement(MEMBERS , null, null).addTextNode(Member);    
                                   
                                  PakageXMLString.add(  '&lt;members&gt;'+Member +'&lt;/members&gt; \n');
                             }
                         
                            types.addChildElement(NAME , null, null).addTextNode(component);
                           
                            PakageXMLString.add( '&lt;name&gt;' + component +'&lt;/name&gt; \n');
                         
                            PakageXMLString.add( '&lt;/types&gt; \n');
                   
                   }
                   
                     //Name Element
                     mypackage.addChildElement(VERSION , null, null).addTextNode(VERSION_NO);
        
                    //Package.xml String
                     String_PackageXMl = doc.toXmlString();
                    
                    //Processing string for getting desired Package.xml string value
                     String_PackageXMl = String_PackageXMl.remove('<?xml version="1.0" encoding="UTF-8"?><ns0:Package xmlns:ns0="http://soap.sforce.com/2006/04/metadata"><fullName>Migration test</fullName>');
                     String_PackageXMl = String_PackageXMl.remove('<version>27.0</version></ns0:Package>');
                  
                   /*
                   for(String component : MapNameWithMembers.keySet()){
                   
                     PakageXMLString =  '\n'+ PakageXMLString  + '&lt;types&gt; \n';
                     for(String Member : MapNameWithMembers.get(component)){
                      
                     PakageXMLString  = '\n' + PakageXMLString + ''+Member +' \n';
                     }
                     
                     PakageXMLString =  '\n' +PakageXMLString + '' + component +' \n';
                     PakageXMLString =  '\n' +PakageXMLString + '&lt;types&gt;';
                   
                   }*/
        
             } 

         }}