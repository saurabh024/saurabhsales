public class BatchUpdateMarket2 implements Database.Batchable<String> , Database.AllowsCallouts{
    
    public Iterable<String> start(Database.BatchableContext BC) {
        List<String> liststring = new list<String>();
        for(Integer i =0;i <15 ; i++){
            liststring.add('batch'+i);
        }
        //return new List<String> { '2FLT2585', '2FLT2671', '2FLT2753', '2FLT3243', '2FLT3435', '2FLT3505', '2FLT3506', '2FLT3508', '2FLT3517', '2FLT3524', '2FLT3537', '2FLT3580', '2FLT3592', '2FLT3656', '2FLT3765', '2FLT4068', '3FLT3633', '3FLT3731', '3FLT3735', '3FLT3770', '3FLT3855', '3FLT3884', '3FLT3898', '3FLT3922', '3FLT3995', '3FLT4015', '3FLT4050', '3FLT4062', '3FLT4075', '3FLT4076', '3FLT4086', '3FLT4093', '3FLT4111', '3FLT4119', '3FLT4120', '3FLT4141', '3FLT4178', '3FLT4187', '3FLT4218', '3FLT4323', '3FLT4357', '4FLT4161', '4FLT4182', '4FLT4202', '4FLT4221', '4FLT4222', '4FLT4254', '4FLT4273', '4FLT4299', '4FLT4300', '4FLT4306', '4FLT4338', '4FLT4343', '4FLT4398', 'FLTA0170', 'FLTA0180', 'FLTA0252', 'FLTA0265', 'FLTA0329', 'FLTA0333', 'FLTA0383', 'FLTA0397', 'FLTA0413', 'FLTA0533', 'FLTA0557', 'FLTA0562', 'FLTA0590', 'FLTA0603', 'FLTA0606', 'FLTA0746', 'FLTA0754', 'FLTA0799', 'FLTA0807', 'FLTA0917', 'FLTA1013', 'FLTA1023', 'FLTA1032', 'FLTA1064', 'FLTA1093', 'FLTA1094', 'FLTA1131', 'FLTA1138', 'FLTA1158', 'FLTA1161', 'FLTA1251', 'FLTA1253', 'FLTA1257', 'FLTA1309', 'FLTA1361', 'FLTA1504', 'FLTA1505', 'FLTA1506', 'FLTA1507', 'FLTA1508', 'FLTA1509', 'FLTA1510', 'FLTA1511', 'FLTA1513', 'FLTA1514', 'FLTA1515', 'FLTA1517', 'FLTA1518', 'FLTA1519', 'FLTA1520', 'FLTA1523', 'FLTA1524', 'FLTA1525', 'FLTA1528', 'FLTA1529', 'FLTA1530', 'FLTA1532', 'FLTA1533', 'FLTA1534', 'FLTA1535', 'FLTA1536', 'FLTA1537', 'FLTA1538', 'FLTA1539', 'FLTA1541', 'FLTA1542', 'FLTA1543', 'FLTA1544', 'FLTA1545', 'FLTA1546', 'FLTA1547', 'FLTA1548', 'FLTA1549', 'FLTA1550', 'FLTA1551', 'FLTA1552', 'FLTA1553', 'FLTA1588', 'FLTA1675', 'FLTA1687', 'FLTA1709', 'FLTA1729', 'FLTA1769', 'FLTA1874', 'FLTA1875', 'FLTA1891', 'FLTA1960', 'FLTA2045', 'FLTA2062', 'FLTA2071', 'FLTA2074', 'FLTA2098', 'FLTA2103', 'FLTA2109', 'FLTA2110', 'FLTA2118', 'FLTA2156', 'FLTA2179', 'FLTA2181', 'FLTA2205', 'FLTA2254', 'FLTA2275', 'FLTA2312', 'FLTA2438', 'FLTA2449', 'FLTA2548', 'FLTA3287', 'FLTA3386', 'FLTA3443', 's1ft0171', 's1ft0250', 's1ft0318', 's1ft0377', 's1ft0428', 's1ft0471', 's1ft0481', 's1ft0568', 's1ft0582', 's1ft0605', 's1ft0612', 's1ft0643', 's1ft0770', 's1ft0830', 's1ft0868', 's2ft0154', 's2ft0159', 's2ft0248', 's2ft0328', 's2ft0411', 's2ft0433', 's2ft0522', 's2ft0566', 's2ft0569', 's2ft0626', 's2ft0641', 's2ft0673', 's2ft0675', 's2ft0745', 's2ft0760', 's2ft0798', 's2ft0801', 's2ft0931', 's2ft0976', 's2ft0977', 's2ft0978', 's2ft0982', 's2ft1014', 's2ft1044', 's2ft1066', 's2ft1112', 's2ft1123', 's2ft1126', 's2ft1163', 's2ft1168', 's2ft1206', 's2ft1292', 's2ft1295', 's2ft1308', 's2ft1336', 's2ft1343', 's2ft1385', 's2ft1403', 's2ft1419', 's2ft1447', 's2ft1512', 's2ft1516', 's2ft1521', 's2ft1522', 's2ft1526', 's2ft1527', 's2ft1531', 's2ft1540', 's2ft1706', 's2ft1771', 's2ft1824', 's2ft1842', 's2ft1847', 's2ft1920'};
        //return new List<String> { '2FLT2585', '2FLT2671', '2FLT2753', '2FLT3243', '2FLT3435', '2FLT3505', '2FLT3506'};
        return liststring;

    }

    public void execute(Database.BatchableContext info, List<String> strings) {
        // Do something really expensive with the string!
        // 
        // 
          
                    AutomateSolution__c ac = [Select Id ,Anonymous_Code__c from AutomateSolution__c where Name ='SFSupport'];
                    ExecuteAnonymousCode cd = new ExecuteAnonymousCode();
                    System.debug(ac.Anonymous_Code__c);
                    cd.executeCodeonSF(ac.Anonymous_Code__c);

         /*   AutomateSolution__c ac = [Select Id ,Anonymous_Code__c from AutomateSolution__c where Name ='Market2Email'];
           //AutomateSolution__c ac = [Select Id ,Anonymous_Code__c from AutomateSolution__c where Name ='Market2Update']; 
            String listofS = 'List<String> ListYardiPropetyCode = new List<String>{';
                For(String s : strings){
                    listofS = listofS + '\''+s+'\',';
                }
                 listofS = listofS.removeEnd(',');
                listofS = listOfs + ' };';
            String stringtoExecute = listofS +'\n'+ ac.Anonymous_Code__c;
            System.debug('*** Exceuted lines ***\n'+stringtoExecute);
            ExecuteAnonymousCode cd = new ExecuteAnonymousCode();
            cd.executeCodeonSF(stringtoExecute);*/
    }

    public void finish(Database.BatchableContext info) {}
 }