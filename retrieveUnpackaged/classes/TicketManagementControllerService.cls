global class TicketManagementControllerService {

 public List<Concierge__c> concierges{get{
    if(concierges == null ){
        concierges = [select Ext_Id__c ,Name,Created_Date__c,Status__c,Region__c,Summary__c ,Owner_Name__c, Requestor_Name__c,Request_Area__c, Request_Type__c,Description__c from concierge__c  where RecordTypeId = :UtilityClass.Concierge_Ticket ];
    }
    return concierges;
    } set;} 
    
    
    
    public String actionList{get{
    
    return JSON.serializePretty([select Name from AutomateSolution__c]);
    } set;} 
    
    
    
    
      public  string JsonConcierges{get{
    
        if(JsonConcierges==null)
        {
         JsonConcierges = JSON.serializePretty(concierges);
        }
        return JsonConcierges;
           } set;}         
            
    //wrapperObject for Concierge 
global  class Concierge{
        
        global string number1;
        global string short_description;
        global string sys_id ;
        global string assignment_group;
        global String sys_created_on;
        global String state;
        global string assigned_to;
        global string closed_at;
        global string location;
        global string subcategory;
        global string category;
        global string caller_id;
        global string closed_by;
        global string parent;
        global string comments_and_work_notes;
        global string description;
        global string request;
        global string request_item;
       
                
        global  Concierge(string number1,string short_description,string sys_id,
        string assignment_group,String sys_created_on,String    state,
        string  assigned_to,string closed_at, string location, string subcategory, 
        string category ,string caller_id , string closed_by ,string parent,string comments_and_work_notes,string description,string request,string request_item){
            
            this.number1 = number1;
            this.short_description = short_description;
            this.sys_id = sys_id;
            this.assignment_group = assignment_group;
            this.sys_created_on =sys_created_on;
            this.state =state;
            this.assigned_to =assigned_to;  
            this.closed_at = closed_at;
            this.location = location;
            this.subcategory = subcategory;
            this.category = category;
            this.caller_id = caller_id;
            this.closed_by = closed_by;
            this.parent = parent;
            this.comments_and_work_notes = comments_and_work_notes;
            this.description = description;
            this.request = request ;
            this.request_item = request_item;
        }
        
}   

    
    
    public void refreshFromServer(){
           
             
             list<concierge__c> conciergetoDelete = [select id ,Ext_Id__c from concierge__c where RecordTypeId = :UtilityClass.Concierge_Ticket];
             Set<String> conciergeSaved = new Set<String>();
             
             for(concierge__c thisConcierge : conciergetoDelete){
              conciergeSaved.add(thisConcierge.Ext_Id__c);
             }
             List<Concierge__c> conciergesToinsert = new list<Concierge__c>();
            // Date searchDate = Date.newInstance(2015,11,24);
             Set<String> conciergeId = new Set<String>();
            // SavePoint sp1 = Database.setSavePoint();
    
             //Database.commit(sp1);
            
            ConciergeReportForIH cr = new ConciergeReportForIH(); 
            List<String> logginDetail = cr.getSessionID();
            partnerSoapSforceCom.Soap sp= new partnerSoapSforceCom.Soap();
            sp.endpoint_x= logginDetail[0];
            partnerSoapSforceCom.SessionHeader_element header= new    partnerSoapSforceCom.SessionHeader_element();
            header.sessionId=logginDetail[1];
            sp.SessionHeader= header;
            string myQuery = 'select id , name ,CreatedDate,Closed_Date__c,Summary__c ,Status__c,Region__c ,ownerId ,Request_Area__c, Request_Type__c,Description__c from Concierge__c where  RecordType.DeveloperName =\'SF_Request\' and Status__c =\'New\' ORDER BY CreatedDate DESC limit 100 ';
             
             partnerSoapSforceCom.QueryResult  ConciergeResult = sp.query(myQuery);
            sobjectPartnerSoapSforceCom.sObject_x[] ConciergeRecord = ConciergeResult.records;
             system.debug(ConciergeRecord);
            
            
            if( ConciergeRecord !=null && !ConciergeRecord.isEmpty()){
            
                DateTime  todayDate  = DateTime.now().addDays(-1);
                
                String dayValue = todayDate.format('EEEE');
                system.debug('day is '+ dayValue);
                if(dayValue.equalsIgnoreCase('Sunday')){
                todayDate = todayDate.addDays(-2);
                }
                else if(dayValue.equalsIgnoreCase('Saturday')){
                
                 todayDate = todayDate.addDays(-1);
                }
                
             
             for(sobjectPartnerSoapSforceCom.sObject_x sob : ConciergeRecord){
             
                   if(conciergeId.contains(sob.id))
                      continue;
                     
                     
                    conciergeId.add(sob.id);
                  
                   
                        
                        Concierge__c crg = new Concierge__c();
                        crg.Ext_Id__c = sob.id;
                        crg.name = sob.name;
                        crg.status__c = sob.status;
                        crg.Closed_Date__c = sob.closedDate;
                        crg.Created_Date__c= sob.CreatedDate.date();
                        crg.RecordTypeId = UtilityClass.Concierge_Ticket;
                        crg.Description__c = sob.Description != null ?sob.Description.replaceAll('<[^>]+>','\n') : sob.Description;           
                        crg.OtherInformation__c = sob.OtherInformation != null ?sob.OtherInformation.replaceAll('<[^>]+>','\n') : sob.OtherInformation;
                        crg.Region__c = sob.Region;
                        crg.Request_Area__c = sob.RequestArea;
                        crg.Request_Type__c = sob.RequestType;    
                        crg.Summary__c = sob.Summary;   
                        crg.Description_assigned__c = sob.Description != null ?sob.Description.replaceAll('<[^>]+>','\n') : sob.Description;     
                        if(sob.OwnerName == '00GE0000001X62oMAC'|| sob.OwnerName =='005E0000006fjEeIAI'){
                         crg.Owner_Name__c   = (sob.OwnerName == '00GE0000001X62oMAC')? 'SF Support' : 'Saurabh kumar';  
                         conciergesToinsert .add(crg);    
                        }        
                       
                     }
             }
                
            delete conciergetoDelete ;         
            if(conciergesToinsert .size()!=0)
            {
               insert conciergesToinsert ;
               concierges.clear();
               concierges.addAll(conciergesToinsert);
               JsonConcierges = JSON.serializePretty(concierges);
               
             //Savepoint sp1 = Database.setSavepoint();
            }
     
    }   
    
    
    
    public void refreshFromSNServer(){
           
             
             list<concierge__c> conciergetoDelete = [select id ,Ext_Id__c from concierge__c where RecordTypeId = :UtilityClass.Concierge_Ticket];
             Set<String> conciergeSaved = new Set<String>();
             
             //map to hold RequestItem and set of Options
             Map<String,Set<String>> maprequestItem = new Map<String,Set<String>>();
             //map to hold OptionId with OptionValue
             Map<String,String> mapOptionWithValue = new Map<String,String>();
             
             for(concierge__c thisConcierge : conciergetoDelete){
              conciergeSaved.add(thisConcierge.Ext_Id__c);
             }
             List<Concierge__c> conciergesToinsert = new list<Concierge__c>();
            // Date searchDate = Date.newInstance(2015,11,24);
             Set<String> conciergeId = new Set<String>();
            // SavePoint sp1 = Database.setSavePoint();
    
             //Database.commit(sp1);
            
            Http h1 = new Http();
            HttpRequest request = new HttpRequest();
            
                        
            request.setEndpoint('https://invitationhomes.service-now.com/api/now/table/incident?sysparm_query=short_descriptionLIKESalesforce%5EORassignment_group%3D4cb9fb40db34be00442930cf9d9619e6&sysparm_display_value=true&sysparm_exclude_reference_link=true&sysparm_fields=closed_at%2Clocation%2Ccategory%2Csubcategory%2Ccaller_id%2Cassignment_group%2Cnumber%2Csys_created_on%2Cshort_description%2Cdescription%2Csys_id%2Cstate%2Cassigned_to%2Cclosed_by&sysparm_limit=1000');
            request.setMethod('GET');
            
            //Eg. UserName="admin", Password="admin" for this code sample.
            //String user = 'admin';
            //String password = 'Saurabh@024';
            String user = 'skumar';
            String password = 'Why2$Instance';
            
             Blob auth = Blob.valueOf(user+':'+password);
            
             System.debug('Auth: '+ auth);
            
             String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(auth);
             request.setMethod('GET');
             request.setHeader('Authorization', authorizationHeader);
             request.setHeader('Content-Type', 'application/json;charset=UTF-8');
            
             HttpResponse response = h1.send(request);
             system.debug(response.getBody());
             String JsonString = response.getBody();
             
             JsonString = JsonString.replaceAll('number','number1');
             JSONParser parser = JSON.createParser(JsonString);
             
             List<Concierge> listConcierge = new list<Concierge>();
              while (parser.nextToken() != null) {
                // Start at the array of invoices.
                if (parser.getCurrentToken() == JSONToken.START_ARRAY) {
                    while (parser.nextToken() != null) {
                        // Advance to the start object marker to
                        //  find next invoice statement object.
                        if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                            // Read entire invoice object, including its array of line items.
                            Concierge inv = (Concierge)parser.readValueAs(Concierge.class);
                            listConcierge.add(inv);
                            system.debug('Invoice number: ' + inv.number1 );
                           
                            // For debugging purposes, serialize again to verify what was parsed.
                            String s = JSON.serialize(inv);
                            system.debug('Serialized invoice: ' + s);
        
                            // Skip the child start array and start object markers.
                            parser.skipChildren();
                        }
                    }
                }
              } 
              
             //taking data from task
             request.setEndpoint('https://invitationhomes.service-now.com/api/now/table/sc_task?sysparm_query=short_descriptionLIKESalesforce%5EORassignment_group%3D4cb9fb40db34be00442930cf9d9619e6&sysparm_display_value=true&sysparm_exclude_reference_link=true&sysparm_fields=parent%2Cmade_sla%2Cwatch_list%2Csc_catalog%2Cupon_reject%2Csys_updated_on%2Capproval_history%2Cnumber%2Csys_updated_by%2Copened_by%2Cuser_input%2Csys_created_on%2Csys_domain%2Cstate%2Csys_created_by%2Cknowledge%2Corder%2Ccalendar_stc%2Cclosed_at%2Ccmdb_ci%2Cdelivery_plan%2Cimpact%2Cactive%2Cwork_notes_list%2Cbusiness_service%2Cpriority%2Csys_domain_path%2Ctime_worked%2Cexpected_start%2Copened_at%2Cbusiness_duration%2Cgroup_list%2Cwork_end%2Capproval_set%2Cwork_notes%2Crequest%2Cshort_description%2Ccorrelation_display%2Cdelivery_task%2Cwork_start%2Cassignment_group%2Cadditional_assignee_list%2Cdescription%2Ccalendar_duration%2Csys_class_name%2Cclose_notes%2Cclosed_by%2Cfollow_up%2Csys_id%2Ccontact_type%2Curgency%2Ccompany%2Creassignment_count%2Cactivity_due%2Cassigned_to%2Ccomments%2Csla_due%2Capproval%2Ccomments_and_work_notes%2Cdue_date%2Csys_mod_count%2Crequest_item%2Crequest_item%2Cescalation%2Cupon_approval%2Ccorrelation_id%2Clocation&sysparm_limit=100');           
              
             response = h1.send(request);
             system.debug(response.getBody());
             JsonString = response.getBody();
             
             JsonString = JsonString.replaceAll('number','number1');
             parser = JSON.createParser(JsonString);
             
             
              while (parser.nextToken() != null) {
                // Start at the array of invoices.
                if (parser.getCurrentToken() == JSONToken.START_ARRAY) {
                    while (parser.nextToken() != null) {
                        // Advance to the start object marker to
                        //  find next invoice statement object.
                        if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                            // Read entire invoice object, including its array of line items.
                            Concierge inv = (Concierge)parser.readValueAs(Concierge.class);
                            listConcierge.add(inv);
                            system.debug('Invoice number: ' + inv.number1 );
                           
                            // For debugging purposes, serialize again to verify what was parsed.
                            String s = JSON.serialize(inv);
                            system.debug('Serialized invoice: ' + s);
        
                            // Skip the child start array and start object markers.
                            parser.skipChildren();
                        }
                    }
                }
              }   
            
            //fetching last 2000 records of Sc_Item_option
             //taking data from task
             request.setEndpoint('https://invitationhomes.service-now.com/api/now/table/sc_item_option?sysparm_query=ORDERBYDESCsys_created_on&sysparm_display_value=true&sysparm_exclude_reference_link=true&sysparm_fields=sys_id%2Cvalue%2Corder&sysparm_limit=5000');         
              
             response = h1.send(request);
             system.debug(response.getBody());
             JsonString = response.getBody();
             
             parser = JSON.createParser(JsonString);
             
             while (parser.nextToken() != null) {
              if (parser.getCurrentToken() == JSONToken.START_ARRAY) {
            	while (parser.nextToken() != null) {
                
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && 
                   (parser.getText() == 'sys_id')) {
                   	String SysId = '';
                   	String valueof = ''; 	
                  
                    parser.nextToken();
                   
                	SysId =  parser.getText();
                	parser.nextToken();
                	parser.nextToken();
                	valueof = parser.getText();
                	parser.nextToken();
                	parser.nextToken();
                	valueof = 'Order :'+parser.nextToken() + ' Value = ' + valueof ;
                	mapOptionWithValue.put(SysId,valueOf);
                  }
                 }
                }
             }
             
             //fetching last 2000 records of Sc_Item_mtom
             //taking data from task
             
             //fetching last 2000 records of Sc_Item_option
             //taking data from task
             request.setEndpoint('https://invitationhomes.service-now.com/api/now/table/sc_item_option_mtom?sysparm_query=ORDERBYDESCsys_created_on&sysparm_display_value=true&sysparm_exclude_reference_link=true&sysparm_fields=request_item%2Csc_item_option&sysparm_limit=5000');         
              
             response = h1.send(request);
             system.debug(response.getBody());
             JsonString = response.getBody();
             
             parser = JSON.createParser(JsonString);
             
             while (parser.nextToken() != null) {
              if (parser.getCurrentToken() == JSONToken.START_ARRAY) {
            	while (parser.nextToken() != null) {
                
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && 
                   (parser.getText() == 'request_item')) {
                   	String reqItem = '';
                   	String valueof = ''; 	
                  
                    parser.nextToken();
                   
                	reqItem =  parser.getText();
                	parser.nextToken();
                	parser.nextToken();
                	valueof = parser.getText();
                	If(maprequestItem.containsKey(reqItem)){
                		maprequestItem.get(reqItem).add(valueOf);
                	}else{
                		maprequestItem.put(reqItem , new Set<String>{valueOf});
                	}
                	
                  }
                 }
                }
             }
             
           
           if( listConcierge !=null && !listConcierge.isEmpty()){
        
                DateTime  todayDate  = DateTime.now().addDays(-1);
                String dayValue = todayDate.format('EEEE');
                system.debug('day is '+ dayValue);
                if(dayValue.equalsIgnoreCase('Sunday')){
                todayDate = todayDate.addDays(-2);
                }
                else if(dayValue.equalsIgnoreCase('Saturday')){
                
                 todayDate = todayDate.addDays(-1);
                }
                
             
                for(Concierge sob : listConcierge){
                
               
                 if(conciergeId.contains(sob.sys_id) ||  sob.assigned_to.trim().length() > 1 )
                  continue;
    
                    conciergeId.add(sob.sys_id);
              
               
                    
                    Concierge__c crg = new Concierge__c();
                    crg.Ext_Id__c = sob.sys_id;
                    crg.name = sob.number1;
                    crg.status__c = sob.state;
                    //crg.Closed_Date__c = sob.closedDate;
                    crg.Created_Date__c= ConciergeReportForIHIncident.parsemyString(sob.sys_created_on);
                    crg.RecordTypeId = UtilityClass.Concierge_Ticket; 
                    crg.Closed_Date__c = ConciergeReportForIHIncident.parsemyString(sob.closed_at);
                    crg.Owner_Name__c = 'New' ;
                    //crg.RecordTypeName= sob.ConciergeType;
                    crg.Region__c = sob.location;
                    crg.Request_Area__c = sob.category;
                    crg.Request_Type__c = sob.subcategory;    
                    crg.Summary__c = sob.short_description; 
                    crg.Description__c = sob.Description;                   
                    conciergesToinsert .add(crg);
                 
                 
             
             }
           } 
                
            delete conciergetoDelete ;         
            if(conciergesToinsert .size()!=0)
            {
               insert conciergesToinsert ;
               concierges.clear();
               concierges.addAll(conciergesToinsert);
               JsonConcierges = JSON.serializePretty(concierges);
               
             //Savepoint sp1 = Database.setSavepoint();
            }
     
    }   
    
    @RemoteAction
     Public Static String AssignedIt(String TicketId ,String myId){
     String  result = '';
     sobjectPartnerSoapSforceCom.sObject_x sob = new sobjectPartnerSoapSforceCom.sObject_x();
     sob.type_x ='Concierge__c';
     sob.id = TicketId;
     sob.OwnerName = Label.Saurabh_kumar;
     
     
     ConciergeReportForIH cr = new ConciergeReportForIH(); 
     List<String> logginDetail = cr.getSessionID();
     partnerSoapSforceCom.Soap sp= new partnerSoapSforceCom.Soap();
     sp.endpoint_x= logginDetail[0];
     partnerSoapSforceCom.SessionHeader_element header= new    partnerSoapSforceCom.SessionHeader_element();
     header.sessionId=logginDetail[1];
     sp.SessionHeader= header;
     
     partnerSoapSforceCom.SaveResult[] svResult = sp.update_x( new List<sobjectPartnerSoapSforceCom.sObject_x>{sob} );
     if(svResult != null && svResult[0].success){
     result = 'Completed';
     //Changing current Concierge Record Type to Assigened
     Concierge__c crg = new Concierge__c(id =myId,RecordTypeId = UtilityClass.Concierge_Assigned);
     update crg;
     //fetching Attachement to this ticket if any 
     insertAttachmentForRelatedConcierge(new Set<Id>{TicketId});  
     }
     else{
     result = svResult[0].errors[0].message;
     }
     
     return result;
     }
    
    /*
    @RemoteAction
     Public Static String Assignedto(String TicketId ){
     String  result = '';
     sobjectPartnerSoapSforceCom.sObject_x sob = new sobjectPartnerSoapSforceCom.sObject_x();
     sob.type_x ='Concierge__c';
     sob.id = TicketId;
     sob.OwnerName = Label.Saurabh_kumar;
     
     
     ConciergeReportForIH cr = new ConciergeReportForIH(); 
     List<String> logginDetail = cr.getSessionID();
     partnerSoapSforceCom.Soap sp= new partnerSoapSforceCom.Soap();
     sp.endpoint_x= logginDetail[0];
     partnerSoapSforceCom.SessionHeader_element header= new    partnerSoapSforceCom.SessionHeader_element();
     header.sessionId=logginDetail[1];
     sp.SessionHeader= header;
     
     partnerSoapSforceCom.SaveResult[] svResult = sp.update_x( new List<sobjectPartnerSoapSforceCom.sObject_x>{sob} );
     if(svResult != null && svResult[0].success){
     result = 'Completed';
     //Changing current Concierge Record Type to Assigened
     //Concierge__c crg = new Concierge__c(id =myId,RecordTypeId = UtilityClass.Concierge_Assigned);
     //update crg;
     }
     else{
     result = svResult[0].errors[0].message;
     }
     
     return result;
     }
       */
       
     @Future(Callout = true)
     public static void insertAttachmentForRelatedConcierge(Set<Id> conciergeExtId){
     
        Map<ID,ID> mapExtIDwithID = new Map<ID ,ID>();
        List<Attachment> listAttachment = new List<Attachment>();
        for(Concierge__c concierge : [Select ID ,Ext_Id__c From Concierge__c where Ext_Id__c IN :conciergeExtID]){
            mapExtIDwithID.put(concierge.Ext_Id__c , concierge.ID);
        }
        //fetching all attachment associated with provided Concierge
        ConciergeReportForIH cr = new ConciergeReportForIH(); 
        List<String> logginDetail = cr.getSessionID();
        partnerSoapSforceCom.Soap sp= new partnerSoapSforceCom.Soap();
        sp.endpoint_x= logginDetail[0];
        partnerSoapSforceCom.SessionHeader_element header= new    partnerSoapSforceCom.SessionHeader_element();
        header.sessionId=logginDetail[1];
        sp.SessionHeader= header;
        string myQuery = 'Select id , name, Body, ContentType ,Parentid from Attachment where  Parentid IN (';
        for(Id con :conciergeExtId ){
            myQuery = myQuery + '\''+ con+'\',';
        }
        System.debug('myQuery'+myQuery); 
        myQuery = myQuery.removeEnd(',');
        myQuery = myQuery +')';
         
        System.debug('myQuery'+myQuery); 
        partnerSoapSforceCom.QueryResult  AttachmentResults = sp.query(myQuery);
        sobjectPartnerSoapSforceCom.sObject_x[] AttachmentRecord = AttachmentResults.records;
        system.debug(AttachmentRecord);
        
        if(AttachmentRecord.isEmpty())
        return;
        
        for(sobjectPartnerSoapSforceCom.sObject_x sob : AttachmentRecord){
             
             Attachment  att = new Attachment ();
             att.body = EncodingUtil.base64Decode(sob.Body);
             att.name = sob.name;
             att.ContentType = sob.ContentType;
             att.Parentid = mapExtIDwithID.get(sob.Parentid);
             listAttachment.add(att);    
         }        
        insert listAttachment;               
                     
        }
}