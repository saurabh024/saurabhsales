public without sharing class UpdateCredentialController {

     public String password{ get; set; }

     public String username { get; set; }
     
     public String SecurityToken {get; set;}
    
     public PageReference submit() {
     
       if(userName==null || password == null || username =='' || password == ''){
       ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Field should not be empty when selected'));
        
       }
       else {
        
         Credential_Control__c cc = Credential_Control__c.getOrgDefaults();
         cc.UserName__c = username;
         cc.Password__c = password;
         cc.Security_Token__c = SecurityToken;
         update cc;
         
         return new PageReference('/apex/DSRANDWSR') ;
       
       }
        return null;
    }
}