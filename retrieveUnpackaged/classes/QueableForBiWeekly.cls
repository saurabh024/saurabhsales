/**
 * Created By :- Saurabh Kumar
 * Created Date :-  10/14/2016
 * Descritption :-  this class is to handle all Post copy Script which will run after any Sandbox Created or 
 *                  refreshed .
 *
**/
 public class QueableForBiWeekly implements Queueable ,Database.AllowsCallouts {
  
  //List to hold Various Action 
  public Integer Step ;
  
  
  public QueableForBiWeekly(){
  	 QuableSequenceController__c seq = QuableSequenceController__c.getOrgDefaults();
  	 step = Integer.valueOf(seq.step__c);
  	 seq.step__c = (Decimal)Math.mod(Integer.valueOf(seq.step__c)+1 , 3);
  	 update seq;
  	
  }
  
  
    //Queueable Method Start Here
    //Method to insert sObject Records in SF (SandBox) post Script 
 	public void execute(QueueableContext context) {
 	   
	    System.debug('Step No.'+step);
	    SchdeularForBiWeeklyReport.sendSIRANDTickets(step);
	    
 	}
     
  

}