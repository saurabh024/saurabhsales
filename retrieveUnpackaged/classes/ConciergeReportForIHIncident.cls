//class for making report on concierge in IH org 
global  class ConciergeReportForIHIncident {
public List<Concierge__c> concierges{get{
if(concierges == null ){
concierges = [select Ext_Id__c ,Name,Status__c,Region__c,Summary__c ,Closed_Date__c,Created_Date__c,Owner_Name__c, Requestor_Name__c,Request_Area__c, Request_Type__c from concierge__c  where RecordTypeId = :UtilityClass.Concierge_ServiceNow ];
}
return concierges;
} set;} 

  public  string JsonConcierges{get{

    if(JsonConcierges==null)
    {
     JsonConcierges = JSON.serializePretty(concierges);
    }
    return JsonConcierges;
       } set;}         
global class assignment_group{
    
    global string link{get;set;}
    global String value{get;set;}
    
    global assignment_group(string link ,string value){
        this.link = link;
        this.value = value;
    }
}        
//wrapperObject for Concierge 
global  class Concierge{
        
        global string number1;
        global string short_description;
        global string sys_id ;
        global string assignment_group;
        global String sys_created_on;
        global String state;
        global string assigned_to;
        global string closed_at;
        global string location;
        global string subcategory;
        global string category;
        global string caller_id;
        global string closed_by;
        global string parent;
        global string comments_and_work_notes;
        global string description;
        global string request;
       
                
        global  Concierge(string number1,string short_description,string sys_id,
        string assignment_group,String sys_created_on,String    state,
        string  assigned_to,string closed_at, string location, string subcategory, 
        string category ,string caller_id , string closed_by ,string parent,string comments_and_work_notes,string description,string request){
            
            this.number1 = number1;
            this.short_description = short_description;
            this.sys_id = sys_id;
            this.assignment_group = assignment_group;
            this.sys_created_on =sys_created_on;
            this.state =state;
            this.assigned_to =assigned_to;  
            this.closed_at = closed_at;
            this.location = location;
            this.subcategory = subcategory;
            this.category = category;
            this.caller_id = caller_id;
            this.closed_by = closed_by;
            this.parent = parent;
            this.comments_and_work_notes = comments_and_work_notes;
            this.description = description;
            this.request = request ;
        }
        
}   


//wrapperObject for Concierge 
global  class CatlogTask{
        
        global string number1;
        global string short_description;
        global string sys_id ;
        global string assignment_group;
        global String sys_created_on;
        global String state;
        global string assigned_to;
        global string closed_at;
        global string location;
        global string subcategory;
        global string category;
        global string caller_id;
        global string closed_by;
        global string parent;
        global string comments_and_work_notes;
        global string description;
        global string request;
        
       
                
        global  CatlogTask(string number1,string short_description,string sys_id,
        string assignment_group,String sys_created_on,String    state,
        string  assigned_to,string closed_at, string location, string subcategory, 
        string category ,string caller_id , string closed_by ,string parent,string comments_and_work_notes,string description,string request){
            
            this.number1 = number1;
            this.short_description = short_description;
            this.sys_id = sys_id;
            this.assignment_group = assignment_group;
            this.sys_created_on =sys_created_on;
            this.state =state;
            this.assigned_to =assigned_to;  
            this.closed_at = closed_at;
            this.location = location;
            this.subcategory = subcategory;
            this.category = category;
            this.caller_id = caller_id;
            this.closed_by = closed_by;
            this.parent = parent;
            this.comments_and_work_notes = comments_and_work_notes;
            this.description = description;
            this.request = request ;
        }
        
        
}           
        
        
 // method for loggin into IH Sf org and retrieve session id 
 public  list<String> getSessionID(){
        
        partnerSoapSforceCom.LoginResult loginResult;       
        partnerSoapSforceCom.Soap sp= new partnerSoapSforceCom.Soap();
        List<String> loginInfo = new list<string>();

        String username = UtilityClass.Username;
        String password = UtilityClass.Password + UtilityClass.securityToken;
        loginResult = sp.login(username, password);
        system.debug('loginResult ' + loginResult);
        loginInfo.add(loginResult.serverUrl);
        loginInfo.add(loginResult.sessionId);
        return  loginInfo;
        
 }      
 
 
 public  void retrieveServiceNow(){
    
             list<concierge__c> conciergetoDelete = [select id ,Ext_Id__c from concierge__c where RecordTypeId = :UtilityClass.Concierge_ServiceNow];
             Set<String> conciergeSaved = new Set<String>();
             for(concierge__c thisConcierge : conciergetoDelete){
              conciergeSaved.add(thisConcierge.Ext_Id__c);
             }
             List<Concierge__c> conciergesToinsert = new list<Concierge__c>();
            // Date searchDate = Date.newInstance(2015,11,24);
             Set<String> conciergeId = new Set<String>();
            // SavePoint sp1 = Database.setSavePoint();
    
             //Database.commit(sp1);
    
            Http h1 = new Http();
            HttpRequest request = new HttpRequest();
            
                        
            //request.setEndpoint('https://invitationhomes.service-now.com/api/now/table/incident?sysparm_query=assignment_group%3D4cb9fb40db34be00442930cf9d9619e6&sysparm_display_value=true&sysparm_exclude_reference_link=true^ORDERBYDESCnumber&sysparm_fields=closed_at%2Clocation%2Ccategory%2Csubcategory%2Ccaller_id%2Cassignment_group%2Cnumber%2Csys_created_on%2Cshort_description%2Cdescription%2Csys_id%2Cstate%2Cassigned_to%2Cclosed_by&sysparm_limit=5');
            request.setEndpoint('https://invitationhomes.service-now.com/api/now/table/incident?sysparm_query=assignment_group%3D4cb9fb40db34be00442930cf9d9619e6&sysparm_display_value=true&sysparm_exclude_reference_link=true&sysparm_fields=closed_at%2Clocation%2Ccategory%2Csubcategory%2Ccaller_id%2Cassignment_group%2Cnumber%2Csys_created_on%2Cshort_description%2Cdescription%2Csys_id%2Cstate%2Cassigned_to%2Cclosed_by&sysparm_limit=5000');
            request.setMethod('GET');
            
            //Eg. UserName="admin", Password="admin" for this code sample.
            //String user = 'admin';
            //String password = 'Saurabh@024';
            String user = Label.TabrezServiceNowUser;
            String password = Label.ServiceNowPassword;
            
             Blob auth = Blob.valueOf(user+':'+password);
            
             System.debug('Auth: '+ auth);
            
             String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(auth);
             request.setMethod('GET');
             request.setHeader('Authorization', authorizationHeader);
             request.setHeader('Content-Type', 'application/json;charset=UTF-8');
            
             HttpResponse response = h1.send(request);
             system.debug(response.getBody());
             String JsonString = response.getBody();
             
             //To solve regular expression limit 
             Integer lengthOfSInc = JsonString.length();
             If(lengthOfSInc >100000){
                          String tempJsonString = '';
                          Integer noOfTraverse = lengthOfSInc/100000 +1 ;
                         for(Integer i = 0 ; i< noOfTraverse ; i++){
                             
                           String currentS =  i == noOfTraverse-1?  JsonString.substring(i*100000, lengthOfSInc ) : JsonString.substring(i*100000, (i+1)*100000);  
                           currentS = currentS.replaceAll('number','number1');  
                           tempJsonString = tempJsonString + currentS;
                         }
                         JsonString = tempJsonString;
              }else{
                JsonString = JsonString.replaceAll('number','number1');
             }
             
             
             JSONParser parser = JSON.createParser(JsonString);
             
             List<Concierge> listConcierge = new list<Concierge>();
              while (parser.nextToken() != null) {
                // Start at the array of invoices.
                if (parser.getCurrentToken() == JSONToken.START_ARRAY) {
                    while (parser.nextToken() != null) {
                        // Advance to the start object marker to
                        //  find next invoice statement object.
                        if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                            // Read entire invoice object, including its array of line items.
                            System.debug('Illegal error'+Parser.getText());
                            Concierge inv = (Concierge)parser.readValueAs(Concierge.class);
                            listConcierge.add(inv);
                            system.debug('Invoice number: ' + inv.number1 );
                           
                            // For debugging purposes, serialize again to verify what was parsed.
                            String s = JSON.serialize(inv);
                            system.debug('Serialized invoice: ' + s);
        
                            // Skip the child start array and start object markers.
                            parser.skipChildren();
                        }
                    }
                }
              } 
              
             //taking data from task
             //request.setEndpoint('https://invitationhomes.service-now.com/api/now/table/sc_task?sysparm_query=assignment_group%3D4cb9fb40db34be00442930cf9d9619e6&sysparm_display_value=true&sysparm_exclude_reference_link=true&sysparm_fields=parent%2Cmade_sla%2Cwatch_list%2Csc_catalog%2Cupon_reject%2Csys_updated_on%2Capproval_history%2Cnumber%2Csys_updated_by%2Copened_by%2Cuser_input%2Csys_created_on%2Csys_domain%2Cstate%2Csys_created_by%2Cknowledge%2Corder%2Ccalendar_stc%2Cclosed_at%2Ccmdb_ci%2Cdelivery_plan%2Cimpact%2Cactive%2Cwork_notes_list%2Cbusiness_service%2Cpriority%2Csys_domain_path%2Ctime_worked%2Cexpected_start%2Copened_at%2Cbusiness_duration%2Cgroup_list%2Cwork_end%2Capproval_set%2Cwork_notes%2Crequest%2Cshort_description%2Ccorrelation_display%2Cdelivery_task%2Cwork_start%2Cassignment_group%2Cadditional_assignee_list%2Cdescription%2Ccalendar_duration%2Csys_class_name%2Cclose_notes%2Cclosed_by%2Cfollow_up%2Csys_id%2Ccontact_type%2Curgency%2Ccompany%2Creassignment_count%2Cactivity_due%2Cassigned_to%2Ccomments%2Csla_due%2Capproval%2Ccomments_and_work_notes%2Cdue_date%2Csys_mod_count%2Crequest_item%2Csys_tags%2Cescalation%2Cupon_approval%2Ccorrelation_id%2Clocation&sysparm_limit=500');           
             //request.setEndpoint('https://invitationhomes.service-now.com/api/now/table/sc_task?sysparm_query=assignment_group%3D4cb9fb40db34be00442930cf9d9619e6&sysparm_display_value=true&sysparm_exclude_reference_link=true^ORDERBYDESCnumber&sysparm_fields=parent%2Cmade_sla%2Csys_updated_on%2Cnumber%2Csys_updated_by%2Copened_by%2Csys_created_on%2Csys_domain%2Cstate%2Csys_created_by%2Corder%2Cclosed_at%2Ccmdb_ci%2Cactive%2Cpriority%2Csys_domain_path%2Copened_at%2Cbusiness_duration%2Cgroup_list%2Cwork_end%2Capproval_set%2Crequest%2Cshort_description%2Ccorrelation_display%2Cdelivery_task%2Cwork_start%2Cassignment_group%2Cadditional_assignee_list%2Cdescription%2Ccalendar_duration%2Csys_class_name%2Cclose_notes%2Cclosed_by%2Csys_id%2Ccontact_type%2Curgency%2Ccompany%2Creassignment_count%2Cassigned_to%2Csla_due%2Cdue_date%2Csys_mod_count%2Csys_tags%2Clocation&sysparm_limit=5000');           
             request.setEndpoint('https://invitationhomes.service-now.com/api/now/table/sc_task?sysparm_query=assignment_group%3D4cb9fb40db34be00442930cf9d9619e6&sysparm_display_value=true&sysparm_exclude_reference_link=true&sysparm_fields=parent%2Cmade_sla%2Csys_updated_on%2Cnumber%2Csys_updated_by%2Copened_by%2Csys_created_on%2Csys_domain%2Cstate%2Csys_created_by%2Corder%2Cclosed_at%2Ccmdb_ci%2Cactive%2Cpriority%2Csys_domain_path%2Copened_at%2Cbusiness_duration%2Cgroup_list%2Cwork_end%2Capproval_set%2Crequest%2Cshort_description%2Ccorrelation_display%2Cdelivery_task%2Cwork_start%2Cassignment_group%2Cadditional_assignee_list%2Cdescription%2Ccalendar_duration%2Csys_class_name%2Cclose_notes%2Cclosed_by%2Csys_id%2Ccontact_type%2Curgency%2Ccompany%2Creassignment_count%2Cassigned_to%2Csla_due%2Cdue_date%2Csys_mod_count%2Csys_tags%2Clocation&sysparm_limit=5000');           
             
             response = h1.send(request);
             system.debug(response.getBody());
             JsonString = response.getBody();
             Integer lengthOfS = JsonString.length();
             List<String> listJsonSplit = new List<String>();
             
             If(lengthOfS >100000){
                          String tempJsonString = '';
                          Integer noOfTraverse = lengthOfS/100000 +1 ;
                         for(Integer i = 0 ; i< noOfTraverse ; i++){
                             
                           String currentS =  i == noOfTraverse-1?  JsonString.substring(i*100000, lengthOfS ) : JsonString.substring(i*100000, (i+1)*100000);  
                           currentS = currentS.replaceAll('"number":"','"number1":"');  
                           tempJsonString = tempJsonString + currentS;
                         }
                         JsonString = tempJsonString;
              }else{
                JsonString = JsonString.replaceAll('"number":"','"number1":"');
             }
             
             parser = JSON.createParser(JsonString);
             
             
              while (parser.nextToken() != null) {
                // Start at the array of invoices.
                if (parser.getCurrentToken() == JSONToken.START_ARRAY) {
                    while (parser.nextToken() != null) { 
                        // Advance to the start object marker to
                        //  find next invoice statement object.
                        if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                            // Read entire invoice object, including its array of line items.
                            Concierge inv = (Concierge)parser.readValueAs(Concierge.class);
                            listConcierge.add(inv);
                            system.debug('Invoice number: ' + inv.number1 );
                           
                            // For debugging purposes, serialize again to verify what was parsed.
                            String s = JSON.serialize(inv);
                            system.debug('Serialized invoice: ' + s);
        
                            // Skip the child start array and start object markers.
                            parser.skipChildren();
                        }
                    }
                }
              }   
            
           
           if( listConcierge !=null && !listConcierge.isEmpty()){
        
                DateTime  todayDate  = DateTime.now().addDays(-1);
                String dayValue = todayDate.format('EEEE');
                system.debug('day is '+ dayValue);
                if(dayValue.equalsIgnoreCase('Sunday')){
                todayDate = todayDate.addDays(-2);
                }
                else if(dayValue.equalsIgnoreCase('Saturday')){
                
                 todayDate = todayDate.addDays(-1);
                }
                
             
                for(Concierge sob : listConcierge){
                
               
                 if(conciergeId.contains(sob.sys_id))
                  continue;
    
                     conciergeId.add(sob.sys_id);
              
               
                    
                    Concierge__c crg = new Concierge__c();
                    crg.Ext_Id__c = sob.sys_id;
                    crg.name = sob.number1;
                    crg.status__c = sob.state;
                    //crg.Closed_Date__c = sob.closedDate;
                    crg.Created_Date__c= parsemyString(sob.sys_created_on);
                    crg.RecordTypeId = UtilityClass.Concierge_ServiceNow; 
                    crg.Closed_Date__c = parsemyString(sob.closed_at);

                   // system.debug('sob date' + sob.CreatedDate.addHours(-6) + 'ticket ' + sob.name);
                    //system.debug('date type' +date.today());
                    //(sob.createdDate== date.today()-2 && sob.closedDate == date.today()-1 )|| (crg.Owner_Name__c == 'IH Team' && sob.createdDate== date.today()-1 &&( sob.closedDate == date.today()-1 ||sob.closedDate == null))
                   if(crg.Created_Date__c >= todayDate.date().addDays(UtilityClass.noofdaysbeforetoday) || ( sob.state !='Resolved'  && sob.state !='Closed' && sob.state !='Closed Complete')|| crg.Closed_Date__c== date.today()){   
                   
                   
                   
                     crg.Owner_Name__c = sob.assigned_to== null || sob.assigned_to.trim().length()==0?'Not Assigned' : sob.assigned_to ;
                    
                    //crg.RecordTypeName= sob.ConciergeType;
                    crg.Region__c = sob.location;
                    crg.Request_Area__c = sob.category;
                    crg.Request_Type__c = sob.subcategory;    
                    crg.Summary__c = sob.short_description;                 
                    conciergesToinsert .add(crg);
                 }
                 
             
             }
           } 
             if(conciergesToinsert .size()!=0){
                   insert conciergesToinsert ;
                   concierges.clear();
                   concierges.addAll(conciergesToinsert);
                   JsonConcierges = JSON.serializePretty(concierges);
                   delete conciergetoDelete ;
                 //Savepoint sp1 = Database.setSavepoint();
            }
             
            
    
 }
 
 public void retrieveConcierge()
 {       
         
         list<concierge__c> conciergetoDelete = [select id ,Ext_Id__c from concierge__c where RecordTypeId = :UtilityClass.Concierge_DSR];
         Set<String> conciergeSaved = new Set<String>();
         for(concierge__c thisConcierge : conciergetoDelete){
          conciergeSaved.add(thisConcierge.Ext_Id__c);
         }
         List<Concierge__c> conciergesToinsert = new list<Concierge__c>();
        // Date searchDate = Date.newInstance(2015,11,24);
         Set<String> conciergeId = new Set<String>();
        // SavePoint sp1 = Database.setSavePoint();

         //Database.commit(sp1);
         
        List<String> logginDetail = getSessionID();
        partnerSoapSforceCom.Soap sp= new partnerSoapSforceCom.Soap();
        sp.endpoint_x= logginDetail[0];
        partnerSoapSforceCom.SessionHeader_element header= new    partnerSoapSforceCom.SessionHeader_element();
        header.sessionId=logginDetail[1];
        sp.SessionHeader= header;
        string myQuery = 'select id , name ,CreatedDate,Closed_Date__c,Summary__c ,Status__c,Region__c ,ownerId ,Request_Area__c, Request_Type__c from Concierge__c where  RecordType.DeveloperName =\'SF_Request\' ORDER BY CreatedDate DESC limit 100 ';
        string myQuery1 = 'select id , name ,CreatedDate,Closed_Date__c,Summary__c,Status__c,Region__c ,ownerId ,Request_Area__c, Request_Type__c from Concierge__c where  RecordType.DeveloperName =\'SF_Request\'  AND (Request_Area__c = \'Support Issue\' OR  Request_Area__c = \'User Access\' ) And  Request_Type__c != \'DATA CHANGES\' And Status__c != \'Closed\' And Status__c != \'Completed\' ';
        string myQuery2 = 'select id , name ,CreatedDate,Closed_Date__c,Summary__c,Status__c,Region__c ,ownerId ,Request_Area__c, Request_Type__c from Concierge__c where  RecordType.DeveloperName =\'SF_Request\'  AND Closed_Date__c = TODAY';
        partnerSoapSforceCom.QueryResult  ConciergeResult = sp.query(myQuery);
        sobjectPartnerSoapSforceCom.sObject_x[] ConciergeRecord = ConciergeResult.records;
         system.debug(ConciergeRecord);
        partnerSoapSforceCom.QueryResult  ConciergeResult1 = sp.query(myQuery1 );
        sobjectPartnerSoapSforceCom.sObject_x[] ConciergeRecord1 = ConciergeResult1.records;
        system.debug(ConciergeRecord1);
         ConciergeRecord.addAll(ConciergeRecord1);
         
         partnerSoapSforceCom.QueryResult  ConciergeResult2 = sp.query(myQuery2 );
         sobjectPartnerSoapSforceCom.sObject_x[] ConciergeRecord2 = ConciergeResult2.records;
         system.debug(ConciergeRecord2);
         if(ConciergeRecord2 != null)
         ConciergeRecord.addAll(ConciergeRecord2);
        
        
        if( ConciergeRecord !=null && !ConciergeRecord.isEmpty()){
        
            DateTime  todayDate  = DateTime.now().addDays(-1);
            String dayValue = todayDate.format('EEEE');
            system.debug('day is '+ dayValue);
            if(dayValue.equalsIgnoreCase('Sunday')){
            todayDate = todayDate.addDays(-2);
            }
            else if(dayValue.equalsIgnoreCase('Saturday')){
            
             todayDate = todayDate.addDays(-1);
            }
            
         
         for(sobjectPartnerSoapSforceCom.sObject_x sob : ConciergeRecord){
         
               if(conciergeId.contains(sob.id))
                  continue;
                 
                 
                conciergeId.add(sob.id);
              
               
                    
                    Concierge__c crg = new Concierge__c();
                    crg.Ext_Id__c = sob.id;
                    crg.name = sob.name;
                    crg.status__c = sob.status;
                    crg.Closed_Date__c = sob.closedDate;
                    crg.Created_Date__c= sob.CreatedDate.date();
                    crg.RecordTypeId = UtilityClass.Concierge_DSR;
                    
                    
                   // system.debug('sob date' + sob.CreatedDate.addHours(-6) + 'ticket ' + sob.name);
                    //system.debug('date type' +date.today());
                    //(sob.createdDate== date.today()-2 && sob.closedDate == date.today()-1 )|| (crg.Owner_Name__c == 'IH Team' && sob.createdDate== date.today()-1 &&( sob.closedDate == date.today()-1 ||sob.closedDate == null))
                   if(crg.Created_Date__c >= todayDate.date()  ||  sob.status !='Closed'||crg.Closed_Date__c== date.today()){   
                   
                   
                   // if(sob.OwnerName == '005E0000006GIHxIAO')005E0000007MD8GIAW
                    if(sob.OwnerName == '005E0000007MD8GIAW')
                    crg.Owner_Name__c   =  'Ashish Varma';
                    else if(sob.OwnerName == '005E0000006fjEeIAI')
                    crg.Owner_Name__c = 'Saurabh Kumar';
                    else if (sob.OwnerName =='005E0000001qfIQIAY'||sob.OwnerName =='005E0000000JyO1IAK'||sob.OwnerName=='005E0000001sp7kIAA'||sob.OwnerName=='005E0000000K7OAIA0')
                    crg.Owner_Name__c = 'Bhavi\'s Team';
                    else if (sob.OwnerName =='005E0000003RcUUIA0')
                    crg.Owner_Name__c = 'Naveen Skaria';
                    else{
                     crg.Owner_Name__c = 'IH Team';
                    }
                    //crg.RecordTypeName= sob.ConciergeType;
                    crg.Region__c = sob.Region;
                    crg.Request_Area__c = sob.RequestArea;
                    crg.Request_Type__c = sob.RequestType;    
                    crg.Summary__c = sob.Summary;                 
                     conciergesToinsert .add(crg);
                 }
         }
                
        }
        if(conciergesToinsert .size()!=0)
        {
           insert conciergesToinsert ;
           concierges.clear();
           concierges.addAll(conciergesToinsert);
           JsonConcierges = JSON.serializePretty(concierges);
           delete conciergetoDelete ;
         //Savepoint sp1 = Database.setSavepoint();
        }
 } 
 public Static Date parsemyString(String datetimestring){
    System.debug('pre'+datetimestring);
    datetimestring = datetimestring.substringBeforeLast(':') +' ' + datetimestring.right(2);  
    datetimestring = datetimestring.replace('-' ,'/');
    System.debug('after'+datetimestring);
  try{
      Datetime mydt =  DateTime.parse(datetimestring);
      return mydt.Date();
    }catch(Exception e){
        return null;
  }
 }
 
 public static void retrieveTask(){
    
            Http h1 = new Http();
            HttpRequest request = new HttpRequest();
            
                        
            request.setEndpoint('https://invitationhomes.service-now.com/api/now/table/sc_task?sysparm_query=assignment_group%3D4cb9fb40db34be00442930cf9d9619e6&sysparm_display_value=true&sysparm_exclude_reference_link=true&sysparm_fields=parent%2Cmade_sla%2Cwatch_list%2Csc_catalog%2Cupon_reject%2Csys_updated_on%2Capproval_history%2Cnumber%2Csys_updated_by%2Copened_by%2Cuser_input%2Csys_created_on%2Csys_domain%2Cstate%2Csys_created_by%2Cknowledge%2Corder%2Ccalendar_stc%2Cclosed_at%2Ccmdb_ci%2Cdelivery_plan%2Cimpact%2Cactive%2Cwork_notes_list%2Cbusiness_service%2Cpriority%2Csys_domain_path%2Ctime_worked%2Cexpected_start%2Copened_at%2Cbusiness_duration%2Cgroup_list%2Cwork_end%2Capproval_set%2Cwork_notes%2Crequest%2Cshort_description%2Ccorrelation_display%2Cdelivery_task%2Cwork_start%2Cassignment_group%2Cadditional_assignee_list%2Cdescription%2Ccalendar_duration%2Csys_class_name%2Cclose_notes%2Cclosed_by%2Cfollow_up%2Csys_id%2Ccontact_type%2Curgency%2Ccompany%2Creassignment_count%2Cactivity_due%2Cassigned_to%2Ccomments%2Csla_due%2Capproval%2Ccomments_and_work_notes%2Cdue_date%2Csys_mod_count%2Crequest_item%2Csys_tags%2Cescalation%2Cupon_approval%2Ccorrelation_id%2Clocation&sysparm_limit=100');           
            request.setMethod('GET');
            
            //Eg. UserName="admin", Password="admin" for this code sample.
            //String user = 'admin';
            //String password = 'Saurabh@024';
            String user = 'skumar';
            String password = 'Why2$Instance';
            
             Blob auth = Blob.valueOf(user+':'+password);
            
             System.debug('Auth: '+ auth);
            
             String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(auth);
             request.setMethod('GET');
             request.setHeader('Authorization', authorizationHeader);
             request.setHeader('Content-Type', 'application/json;charset=UTF-8');
            
             HttpResponse response = h1.send(request);
             system.debug(response.getBody());
             String JsonString = response.getBody();
             system.debug('JosonString'+JsonString);
             
             JsonString = JsonString.replaceAll('number','number1');
             JSONParser parser = JSON.createParser(JsonString);
             
             List<Concierge> listConcierge = new list<Concierge>();
              while (parser.nextToken() != null) {
                // Start at the array of invoices.
                if (parser.getCurrentToken() == JSONToken.START_ARRAY) {
                    while (parser.nextToken() != null) {
                        // Advance to the start object marker to
                        //  find next invoice statement object.
                        if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                            // Read entire invoice object, including its array of line items.
                            Concierge inv = (Concierge)parser.readValueAs(Concierge.class);
                            listConcierge.add(inv);
                            system.debug('Task number: ' + inv.number1 );
                           
                            // For debugging purposes, serialize again to verify what was parsed.
                            String s = JSON.serialize(inv);
                            system.debug('Serialized invoice: ' + s);
        
                            // Skip the child start array and start object markers.
                            parser.skipChildren();
                        }
                    }
                }
              }   
  }
 
}