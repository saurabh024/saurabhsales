/*
 *  this class for finding all user directly or indirectly related to the group name provided .
 *
 */

public with sharing class MetadataControllerForGroup {
	
	public  static string sessionidSF ='';
	public  static string urlSoap = '';
	public  static string urlMetadata = '';
	
	
	//Method to fetch all Group (through sharing setting ),Profile ,Permission Set which provided access to mentioned object 
	//with record type 
	public Map<String ,Set<String>> getrealatedEntity(String sobjectName ,string recordTypename){
        
        list<String> loginDetails = getLoginDetails('skumar@invitationhomes.com.uat' ,'Cybage5invitation');
		Map<String ,Set<String>> mapEntityNameWithrelatedEntity = new Map<String, Set<String>>();
		
		//Read  all property of provided sobject 
		MetadataService.MetadataPort ms = createService();
		MetadataService.IReadResult metadataread = ms.readMetadata('CustomObject',new List<String>{sobjectName});
		MetadataService.CustomObject[] customObjs  = (MetadataService.CustomObject[])metadataread.getRecords();
		if(customObjs == null)
		return null;
		
		MetadataService.CustomObject objectdescription = customObjs[0];
		System.debug('OWD IS '+objectdescription.sharingModel);
		
		If(objectdescription.sharingModel.equalsIgnoreCase('Private')){
			//qurey all sharingRules on this object 
		    set<String> setGroup = new Set<String>();
		    set<String> setRole  = new Set<String>();
			MetadataService.IReadResult metadatareadsharing = ms.readMetadata('SharingRules',new List<String>{sobjectName});
			MetadataService.SharingRules[] sharingRules  = (MetadataService.SharingRules[])metadatareadsharing.getRecords();
			//System.debug(sharingRules);
			for(MetadataService.SharingCriteriaRule scriteria : sharingRules[0].sharingCriteriaRules){
				   //System.debug('sharedTo'+scriteria.sharedTo);
					if(scriteria.criteriaItems == null || scriteria.sharedTo == null )
					continue;
					Boolean recordTypeMentioned = false;
					
					for( MetadataService.FilterItem filter : scriteria.criteriaItems){
						 //System.debug('filter'+filter);
						 
						 If( filter.field == 'RecordTypeId'){
						 	
						 	If(filter.value.contains(recordTypename)){
							 	If(scriteria.sharedTo.group_x != null)
							 	setGroup.addAll(scriteria.sharedTo.group_x);
							 	If(scriteria.sharedTo.roleAndSubordinatesInternal != null)
							 	setRole.addALL(scriteria.sharedTo.roleAndSubordinatesInternal);
						 	 }
						 	 recordTypeMentioned = true;
							 break;
						  }
					 }
					 
					 If(!recordTypeMentioned){
					 	If(scriteria.sharedTo.group_x != null)
					 	setGroup.addAll(scriteria.sharedTo.group_x);
					    If(scriteria.sharedTo.roleAndSubordinatesInternal != null)
					 	setRole.addALL(scriteria.sharedTo.roleAndSubordinatesInternal);
			 	
					 }
					 
					 
	         }
	         
	        // System.debug('Total Group having access to this recordType '+recordTypeName +'***'+setGroup);
	           mapEntityNameWithrelatedEntity.put('Group' ,setGroup);
	           mapEntityNameWithrelatedEntity.put('roleAndSubordinatesInternal' ,setRole);
	            //System.debug('Total Group having access to this recordType '+recordTypeName +'***'+setGroup);
			
	    }
		
		//System.debug(customObjs);
       Map<String , Map<String , String>> metadataaccess =  getRelatedProfilePermmision(sobjectName);
       mapEntityNameWithrelatedEntity.put('Profile' ,metadataaccess.get('Profile').keySet());
       mapEntityNameWithrelatedEntity.put('PermmissionSet' ,metadataaccess.get('PermmissionSet').keySet());
       
	   return mapEntityNameWithrelatedEntity;
	}
	
	
	//Rest Call to another SF org to fetch all Profile and Permission set having access to perticaulr sobject 
	public Map<String , Map<String , String>> getRelatedProfilePermmision(String sobjectName){
		 
		 
		 list<String> loginDetails = getLoginDetails('skumar@invitationhomes.com.uat' ,'Cybage5invitation');
		 String baseurl = getBaseUrl(urlSoap);
		 String separator = ',';
		 Map<String ,String> mapProfileIdtoPermissionDetails = new Map<String,String>();
		 Map<String, String> mapPermissionIdToPermissionDetails = new Map<String, String>();
		 Map<String , Map<String , String>> mapResult = new  Map<String , Map<String , String>>();
		// String query = 'SELECT Id,PermissionsDelete,PermissionsCreate ,PermissionsRead ,PermissionsEdit ,Parent.Profile.Name,Parent.Id ,Parent.Profile.Id, SobjectType ,Parent.IsOwnedByProfile FROM ObjectPermissions WHERE   PermissionsRead = TRUE And SobjectType = \''+sobjectName+'\'';
		 PageReference   urlPg   = new PageReference(baseurl+ '/services/data/v35.0/query');
         urlPg.getParameters().put('q','SELECT Id,PermissionsDelete,PermissionsCreate ,PermissionsRead ,PermissionsEdit ,Parent.Profile.Name,Parent.Id ,Parent.Profile.Id, SobjectType ,Parent.IsOwnedByProfile FROM ObjectPermissions WHERE   PermissionsRead = TRUE And SobjectType = \''+sobjectName+'\''); 
 
         String uri              = urlPg.getUrl(); 
		 
		 string query ='Select Id , Name from Account limit 10';
		 Http http = new Http();
		 HttpRequest request = new HttpRequest();
		 request.setMethod('GET');
		 request.setEndPoint(uri);
		 //request.setEndPoint(baseurl+'/services/data/v35.0/query/?q='+query);
		 //'Authorization','Bearer ' +
		 //request.setHeader('X-SFDC-Session' , sessionidSF);
		 //request.setHeader('X-SFDC-Session' , +sessionidSF);
		 request.setHeader('Authorization' ,'Bearer ' +sessionidSF);
		 System.debug(request);
		 HttpResponse response = http.send(request);
		 String body = response.getBody();
		 System.debug('BODY '+body);
		 //Map<String, String> m = (Map<String, String>)JSON.deserializeUntyped(response.getBody());
		 
		 JSONParser jp = JSON.createParser(body);
         //Map<String,String> resultquery = (Map<String,String>) JSON.deserialize(resq.getBody(),Map<String,String>.class);
         system.debug(jp);
        do{
            jp.nextToken();
            
         } while(jp.hasCurrentToken() && !'records'.equals(jp.getCurrentName()));
         jp.nextToken();  // token is 'records'
         List<sObject> listObjectPermission = (List<sObject>) jp.readValueAs(List<sObject>.class);
		 // List<ObjectPermissions> listObjectPermission = (List<ObjectPermissions>)JSON.deserialize(records ,List<ObjectPermissions>.class);
		 System.debug(listObjectPermission);
		 
		 For(sObject permissionDetail : listObjectPermission){ 
    			Boolean isowned = (Boolean)permissionDetail.getSobject('Parent').get('IsOwnedByProfile');
				If(isowned){
				mapProfileIdtoPermissionDetails.put((String)permissionDetail.getSobject('Parent').getSobject('Profile').get('Name') , ''+permissionDetail.get('SobjectType')+separator+permissionDetail.get('PermissionsRead')+separator+permissionDetail.get('PermissionsEdit')+separator+permissionDetail.get('PermissionsCreate')+separator+permissionDetail.get('PermissionsDelete'));
				}
				else{
				mapPermissionIdToPermissionDetails.put((String)permissionDetail.getSobject('Parent').get('Id') , ''+permissionDetail.get('SobjectType')+separator+permissionDetail.get('PermissionsRead')+separator+permissionDetail.get('PermissionsEdit')+separator+permissionDetail.get('PermissionsCreate')+separator+permissionDetail.get('PermissionsDelete'));
				}
		  }
          mapResult.put('Profile' ,mapProfileIdtoPermissionDetails);
          mapResult.put('PermmissionSet' ,mapPermissionIdToPermissionDetails);
		 
		 return mapResult;
		 
		 
	}
	
	
	
	
	
	
	static{
		UtilityClass.sfLoginURL = 'https://test.salesforce.com/services/Soap/u/35.0';
		//getLoginDetails('skumar@invitationhomes.com.uat' ,'Cybage5invitation');
		
	}
	
	//getting sessionId ,Soap URL ,MetadataURl for external system 
	public  List<String>  getLoginDetails(String UserName ,String Password){
		
		UtilityClass.sfLoginURL = 'https://test.salesforce.com/services/Soap/u/35.0';
		partnerSoapSforceCom.LoginResult loginResult;       
        partnerSoapSforceCom.Soap sp= new partnerSoapSforceCom.Soap();
        List<String> loginInfo = new list<string>();
        //String username = 'skumar@invitationhomes.com.uat';
        //String password = 'Cybage4invitation';
        If(!isSessionActive()){
        loginResult = sp.login(username, password);
        //system.debug('loginResult ' + loginResult);
        sessionidSF = loginResult.sessionId;
		urlSoap = loginResult.serverUrl;
	    urlMetadata = loginResult.metadataServerUrl;
        }
        loginInfo.add(sessionidSF); 
        loginInfo.add(urlSoap);
        loginInfo.add(urlMetadata);
       // System.debug('loginInfo '+loginInfo);
        return  loginInfo;
   }
   
   
   //test sessionActiveOrnot
   public  Boolean isSessionActive(){
   	
   	 MetadataService.MetadataPort ms = createService();
   	 try{
   	 MetadataService.DescribeMetadataResult rs = ms.describeMetadata(39);
   	 }catch(Exception e){
   	 	return false;
   	 }
   	 
   	 return true;
   	 
   }
    
  public  MetadataService.MetadataPort createService()
    {
    	
        MetadataService.MetadataPort service = new MetadataService.MetadataPort();
        service.endpoint_x = urlMetadata;
        service.SessionHeader = new MetadataService.SessionHeader_element();
        service.SessionHeader.sessionId = sessionidSF ;
        return service;
    } 
    
    
  //method to fetc base url
  public string getBaseUrl(String url){
  	
   		Pattern pt = Pattern.compile('(([a-z]{3,9})(:)(//)([a-z0-9])+(.)(?:[a-z]{0,12})+(.)([a-z]{0,3}))');
        Matcher matcher = pt.matcher(url);
        String surl = '';
        while(matcher.find()){
	         surl = matcher.group();
	         System.debug(surl);
	         If(surl != '' && surl != null)
	         break;
         
        } 	
        return surl;
  }
  
     
}