global  class AssignedTicketManagement {

public Map<Id,Concierge__c> concierges{get{
if(concierges == null ){
concierges = new Map<Id,Concierge__c>([select Id, Name,Created_Date__c,Status__c,Region__c,Summary__c ,Owner_Name__c, Requestor_Name__c,Request_Area__c, OtherInformation__c ,Request_Type__c,Description_assigned__c,Resolution_Note__c, (Select id,Name from Attachments)  from concierge__c  where RecordTypeId = :UtilityClass.Concierge_Assigned ]);
}
return concierges;
} set;} 




  public  string JsonConcierges{get{

    if(JsonConcierges==null)
    {
     JsonConcierges = JSON.serializePretty(concierges.values());
    }
    return JsonConcierges;
       } set;}         
        
//wrapperObject for Concierge 
global  class Concierge{
        
        global string id {get; set;}
        global string name{get; set;}
        global string status {get; set;}
        global string region{get; set;}
        global Date   createdDate{get; set;}
        global Date   closedDate{get; set;}
        global string  OwnerName{get; set;}
        global string RecordTypeName{get; set;}
        global string Description{get; set;}
        
        global  Concierge(){
                
        }
        
        
} 


public PageReference init(){


PageReference pgref = null;
Utility__c utility = Utility__c.getInstance('00e28000000YUQz');
String utility_Password = utility.TicketPassword__c;
    if(ApexPages.currentPage().getParameters() != null && ApexPages.currentPage().getParameters().get('password')!= null && ApexPages.currentPage().getParameters().get('password').equalsIgnoreCase(utility_Password)){
        return null;
        
    }else{
        pgref = new PageReference('/apex/loginTicket');
        return pgref;
    }
}

 @RemoteAction
 Public Static String saveit(String Id ,String Resolution){
 String  result = '';
 
 Concierge__c crg = new Concierge__c(id =Id ,Resolution_Note__c = Resolution ,Status__c = 'Closed');
 update crg;
 return result;
 }

@RemoteAction
 Public Static String deleteIt(String Id ){
 String  result = '';
 
     try{
         Concierge__c crg = [select id from Concierge__c where Id = :Id];
         delete crg;
         result = 'Success';
     }catch(Exception e){
          result = e.getMessage();
     }
 return result;
 }   

    
}