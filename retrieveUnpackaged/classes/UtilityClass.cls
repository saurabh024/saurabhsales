Public   class UtilityClass {


 //method to return no of weekends comes in between 
 public static string  Username =  'skumar@siemens.com'; //'skumar@siemens.com.coedev1';  //Credential_Control__c.getOrgDefaults().Username__c;
    
 public static string  Password  =  'Cybage7personal'; // Credential_Control__c.getOrgDefaults().Password__c;  
 
 public static string  securityToken =   (Credential_Control__c.getOrgDefaults().Security_Token__c == null || Credential_Control__c.getOrgDefaults().Security_Token__c.trim() == '') ? '' : Credential_Control__c.getOrgDefaults().Security_Token__c; 
 
 public static ID  Concierge_DSR = Schema.sObjectType.Concierge__c.getRecordTypeInfosByName().get('DSR').getRecordTypeId();
 public static ID  Concierge_Ticket = Schema.sObjectType.Concierge__c.getRecordTypeInfosByName().get('Ticket').getRecordTypeId(); 
 public static ID  Concierge_Assigned = Schema.sObjectType.Concierge__c.getRecordTypeInfosByName().get('AssignedToDev').getRecordTypeId();    
 public static ID  Concierge_ServiceNow = Schema.sObjectType.Concierge__c.getRecordTypeInfosByName().get('ServiceNowTicket').getRecordTypeId();
 public Date noOfWeekends(Date StartDate  , Integer AdditionDay){
  
 
 Integer NoOfWeekends = 0 ;
 Date startOfWeek = StartDate.toStartOfWeek();
        
  //Days difference count in between the dates
 Integer dayOfWeek  = startOfWeek.daysBetween(StartDate);
 
 NoOfWeekends =  (AdditionDay/5)*2 +  ( (dayOfWeek + Math.mod(AdditionDay , 5)) > 5 ? 2 : 0 ) ;
 AdditionDay = AdditionDay + noOfWeekends ;  
 
 return StartDate.addDays(AdditionDay) ;  
 
 }
    @future
    public static void testFutureMethod(list<String> listquery){
        
        String query = 'Find { ';
        for(String s : listquery){
                query = query +' \"'+ s + '\" or';
            }
        query = query.removeEnd('or');
        query = query + ' } IN ALL FIELDS RETURNING Account(id,name),Contact, Lead';
        
        system.debug('query'+query);
        search.SearchResults searchrs= Search.find(query);
        system.debug('all result '+searchrs);
        for(Search.SearchResult ss : searchrs.get('Account') ){
            
            system.debug('result'+ ss.getSobject());
            
        }
        
   }
    
    //static variables
    public static Integer noofdaysbeforetoday = -1;
    
    
    public static void sendBiweeklyReport(){
    
        String allEmail = Label.BIWEEKLYEMAIL;
        List<String> allEmails = allEmail.split(',');
        
        List<String> listReportName = Label.ReportDevName.split(',');
        
        List<Report> listReport  = [Select Id ,Name ,DeveloperName From Report Where DeveloperName IN :listReportName Order By DeveloperName];
        List<Messaging.EmailFileAttachment> listAttachment = new List<Messaging.EmailFileAttachment>();
       
        
        
        //fetching Reports Data from Report.
        
                    messaging.Singleemailmessage mail = new  Messaging.SingleEmailMessage();
                    
                    String[] toAddresses = allEmails;
                    mail.setToAddresses(toAddresses);
                    mail.setSenderDisplayName('BiWeeklyReport');
                   // Specify the subject line for your email address.
                    mail.setSubject('Bi Weekly Report |IH****');
                   
                    mail.setBccSender(false);   
                  
                    mail.setUseSignature(false);
                               
                    mail.setPlainTextBody(' Hi  kindly find attachment of SIR and Ticktes'  );
                    
                    for(Report report : listReport){
                        //calling pageReference method to fetch CSV of Reports 
                        //PageReference page = new PageReference('/00O2800000ABjRj?rt=32&retURL=%2Fservlet%2Fservlet.ReportList&t=active&c=FN&c=LN&c=RO&c=PR&c=UN&c=LL&duel0=FN%2CLN%2CRO%2CPR%2CUN%2CLL&details=yes&export=1&enc=UTF-8&xf=csv'); 
                        //system.debug('response data=='+page.getContent().toString());
                        Reports.ReportResults results = Reports.ReportManager.runReport(report.Id, true);
                        ApexPages.PageReference reportcsv = new ApexPages.PageReference('/'+report.Id+'?csv=1');
                        //system.debug('response1 data=='+report.getContent().toString());      
                        Blob  b = Blob.valueOf(reportcsv.getContent().toString());
                        Messaging.EmailFileAttachment d = new Messaging.EmailFileAttachment();
                        d.setbody(b); 
                        d.setFileName(report.DeveloperName+'.csv');
                        listAttachment.add(d);
                        
                    }   
  
                    mail.setFileAttachments(listAttachment);
                    List<Messaging.SendEmailResult>  sendEmailResults = new list<Messaging.SendEmailResult>() ;
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }, false);        
            
    
    }
    
    
    public static string sfLoginURL = 'https://login.salesforce.com/services/Soap/u/35.0';
    
    
}