public class BatchUpdateCreateOpp  implements Database.Batchable<String> , Database.AllowsCallouts {
   
    public  integer noofIteration;
     public List<String> logginDetail; 
    public BatchUpdateCreateOpp( ){
      logginDetail = new ExecuteAnonymousCode().getSessionID();
    }
    public Iterable<String> start(Database.BatchableContext BC) {
        
        List<String> liststring = new list<String>{'22036782', '22085885', '22088794', '22096424', '22119515', '22120393', '22121455', '22123156', '22124526', '22124967',
            '22126952', '22139818', '22140182', '22142600', '22145047', '22145740', '22146233', '22148986', '22149919', '22149958',
            '22154866', '22155530', '22157662', '22158912', '22159815', '22161816', '22161844', '22162406', '22162463', '22162689', 
            '22164722', '22164737', '22164862', '22165056', '22165059', '22165307', '22166927', '22168327', '22168749', '22168835', 
            '22169234', '22169307', '22169722', '22169772', '22169774', '22170061', '22170471', '22170563', '22170912', '22170955', 
            '22171921', '22171933', '22172170', '22172860', '22173046', '22173495', '22173554', '22173620', '22174388', '22175291'};
       
        return liststring;
    }

    public void execute(Database.BatchableContext info, List<String> strings) {
       
        
        AutomateSolution__c ac = [Select Id ,Anonymous_Code__c from AutomateSolution__c where Name ='CreateOppAutogen'];   
        
                    
        String stringtoExecute =  ac.Anonymous_Code__c;
                   
        System.debug('*** Exceuted lines ***\n'+stringtoExecute);

           ExecuteAnonymousCode cd = new ExecuteAnonymousCode();          
        
                    
                    System.debug(ac.Anonymous_Code__c);
          cd.executeCodewithLoginDetail(stringtoExecute ,logginDetail);
                  //  cd.executeCodeonSF(stringtoExecute);
        

    }

    public void finish(Database.BatchableContext info) {}
}