/*
* Schedular for updating SIR and Tickets from SF Org and Service Now respectively and send and Email with Report to associated Persons 
*
*/

public with sharing class SchdeularForBiWeeklyReport implements Schedulable {
    
    
     public void execute(SchedulableContext sc) {
        
        
         //calling method to send required csv
         System.enqueueJob(new QueableForBiWeekly());
         
         //deleteing all earlier ScheduleJob for this batch or schedular
          For(CronTrigger ct : [SELECT Id, CronJobDetail.Id, CronJobDetail.Name, CronJobDetail.JobType FROM CronTrigger Where CronJobDetail.Name like 'UpdateSIRTicket%']){
               System.abortJob(ct.ID);
          }
          
          SchdeularForBiWeeklyReport biweeklySchedule = new SchdeularForBiWeeklyReport ();
          DateTime currentTime =  nextBiweeklyMonday();//DateTime.Now().addDays(1);
          
          String cronstring = currentTime.second()+' '+currentTime.minute()+' '+currentTime.hour()+' '+currentTime.day()+' * ?';
          //calling next Schedular after 3 hrs .
          String jobID = system.schedule('UpdateSIRTicket'+cronstring, cronstring, biweeklySchedule);
      
       }
     
        public DateTime nextBiweeklyMonday(){
             
             Date StartDate = Date.Today();
             Date startOfWeek = StartDate.toStartOfWeek();
             System.debug('StartOfWeek'+startOfWeek);
            //Days difference count in between the dates
             Integer dayOfWeek  = startOfWeek.daysBetween(StartDate);
             System.debug('dayOfWeek'+dayOfWeek);
             Date NextScheduledDate = dayOfWeek == 5 ? StartDate.addDays(7): StartOfWeek.addDays(5);
             DateTime NextSchduledDateTime = DateTime.newInstance(NextScheduledDate.year(), NextScheduledDate.month(), NextScheduledDate.day(), 5, 20, 20);
             
             return NextSchduledDateTime;
            
        } 
         
        @future(callout= true)
        public static void sendSIRANDTickets(Integer step){
            
             //updating SIR from org
             If(step == 1){
             ConciergeReportForIH conceirgeReportSIR = new ConciergeReportForIH();
             conceirgeReportSIR.retrieveSIR();
             System.enqueueJob(new QueableForBiWeekly());
             }
             else If(step ==2){
            //updating Tickets/Incidnet/Task 
             UtilityClass.noofdaysbeforetoday = -30;
             ConciergeReportForIHIncident conciergeServiceNow = new ConciergeReportForIHIncident();
             conciergeServiceNow.retrieveServiceNow();
            
             System.enqueueJob(new QueableForBiWeekly());
             }
             //sending email to realted person with attachment of Reports
             else{
                UtilityClass.noofdaysbeforetoday = -1;
                UtilityClass.sendBiweeklyReport();
                QuableSequenceController__c seq = QuableSequenceController__c.getOrgDefaults();
                seq.step__c =1;
                update seq;
             }
        }
}