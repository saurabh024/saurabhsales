public without sharing class loginControl {

    public String Password { get; set; }
    public String ticket_Password { get; set; }
    
   @RemoteAction
    public Static String login_to_SirSite(String Pass){
    
    PageReference pgref = Null;
    String result = 'fail';
    Utility__c utility = Utility__c.getInstance('00e28000000YUQz');
    String utility_Password = utility.Password__c;
    if(Pass!= Null && Pass.equalsIgnoreCase(utility_Password)){
     result = '/apex/PackageXML?password='+Pass +'';
    }    
    return  result ;
    }
    
    
    
    public  PageReference loginToSirSite(){
    
    PageReference pgref = Null;
    //String result = 'fail';
    Utility__c utility = Utility__c.getInstance('00e28000000YUQz');
    String utility_Password = utility.Password__c;
    if(Password!= Null && Password.equalsIgnoreCase(utility_Password)){
        pgref = new PageReference('/apex/PackageXML');
        pgref.getParameters().put('password' ,Password);
        
          }   
      else
      {
        Apexpages.addMessage( new ApexPages.message(ApexPages.severity.INFO ,'Password :- '+Password +' is Wrong , Please Try again'));
            }     
    return  pgref ;
    }

   
   public  PageReference loginToTicketSite(){
    
    PageReference pgref = Null;
    //String result = 'fail';
    Utility__c utility = Utility__c.getInstance('00e28000000YUQz');
    String utility_Password = utility.TicketPassword__c;
    if(ticket_Password != Null && ticket_Password.equalsIgnoreCase(utility_Password)){
        pgref = new PageReference('/apex/AssignedConcierge');
        pgref.getParameters().put('password' ,ticket_Password);
        
        //inserting a record for logged in UserInfo and browserInfo 
        System.debug('Inside REf');
        addLoggedin_Info();
        System.debug('Inside REf1');
          }   
      else
      {
        Apexpages.addMessage( new ApexPages.message(ApexPages.severity.INFO ,'Password :- '+ticket_Password +' is Wrong , Please Try again'));
            }     
    return  pgref ;
    } 
    
    
    
 private void addLoggedin_Info(){
    
    string ReturnValue = '';  
   
 // True-Client-IP has the value when the request is coming via the caching integration.
        ReturnValue = ApexPages.currentPage().getHeaders().get('True-Client-IP');
 
          // X-Salesforce-SIP has the value when no caching integration or via secure URL.
        if (ReturnValue == '' || ReturnValue == null) {
         ReturnValue = ApexPages.currentPage().getHeaders().get('X-Salesforce-SIP');
        } // get IP address when no caching (sandbox, dev, secure urls)
 
         if (ReturnValue == '' || ReturnValue == null) {
         ReturnValue = ApexPages.currentPage().getHeaders().get('X-Forwarded-For');
        } // get IP address from standard header if proxy in use
        
        String deviceType  ='';
        
        String userAgent = ApexPages.currentPage().getHeaders().get('USER-AGENT');
        if(userAgent.contains('iPhone')) 
        deviceType = 'iPhone';
        else if(userAgent.contains('BlackBerry')) 
        deviceType = 'BlackBerry';
        
        //inserting new record 
        LoggedInINFO__c loggedInINFO = new LoggedInINFO__c(IP_Address__c = ReturnValue ,PlatForm__c = deviceType,BrowserName__c =userAgent  ,Logged_In_Time__c = System.Now());
        insert loggedInINFO;
    
    }
}