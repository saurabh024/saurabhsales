public with sharing class PdfController {
  
    public List<Employee__c> listEmployee {get; set;}
    public Map<String , list<Employee__c> > mapMonthwithAttendance {get; set;}
    public Map<String , list<list<Employee__c>> > mapMonthwithAttendance1 {get; set;}
    public Map<String , Decimal> mapMonthwithTotalWorkinghour {get; set;}
    public List<String> keyvalues{get; set;}
    private Integer noOfrowperPage =25;
    public Map<Integer ,String> mapMonth = new Map<Integer ,String> {
    1 =>'Jan', 
    2 => 'Feb',
    3 => 'March', 
    4 => 'April',
    5 => 'May',
    6 =>'June',
    7 =>'July',
    8 =>'Aug',
    9 =>'Sept',
    10 =>'Oct',
    11 =>'Nov',
    12 =>'Dec'
    };


    public PdfController(ApexPages.StandardController controller) {
        mapMonthwithAttendance1  = new Map<String ,List<List<employee__c>>>();
       mapMonthwithAttendance  = new Map<String ,List<employee__c>>();
       mapMonthwithTotalWorkinghour  = new Map<String ,Decimal>();
       keyvalues = new List<String>();
       
      for(Employee__c emp : [select Id , Name ,Company__c ,Date__c ,Absolute_time__c from Employee__c Order by Date__c Asc ]){
       
       String datecomponent =  mapMonth.get(emp.Date__c.month())+ '  ' +emp.Date__c.year();
       if(mapMonthwithAttendance.containskey(datecomponent)){
          
           Integer sizeoflist = mapMonthwithAttendance1.get(datecomponent).size();
           if(mapMonthwithAttendance1.get(datecomponent)[sizeoflist -1].size() == noOfrowperPage){
             mapMonthwithAttendance1.get(datecomponent).add(new List<Employee__c>{emp});
           }
           else{
             mapMonthwithAttendance1.get(datecomponent)[sizeoflist -1].add(emp);
           }
           mapMonthwithAttendance.get(datecomponent).add(emp);
           mapMonthwithTotalWorkinghour.put( datecomponent , mapMonthwithTotalWorkinghour.get(datecomponent) + emp.Absolute_time__c);
       }
       else{
        mapMonthwithAttendance1.put(datecomponent , new List<List<Employee__c>>());
       mapMonthwithAttendance1.get(datecomponent).add(new List<Employee__c>{emp});
       mapMonthwithAttendance.put(datecomponent , new List<Employee__c>{ emp});
       mapMonthwithTotalWorkinghour.put(datecomponent , emp.Absolute_time__c);
       keyvalues.add(datecomponent);
       
       }
        
      }
      
     system.debug('mapMonthwithAttendance' +mapMonthwithAttendance);
     system.debug('mapMonthwithTotalWorkinghour' +mapMonthwithTotalWorkinghour);
     
    }

}