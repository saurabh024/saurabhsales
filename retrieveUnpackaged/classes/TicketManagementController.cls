global class TicketManagementController {

	public List<Concierge__c> concierges{get{
	if(concierges == null ){
		concierges = [select Ext_Id__c ,Name,Created_Date__c,Status__c,Region__c,Summary__c ,Owner_Name__c, Requestor_Name__c,Request_Area__c, Request_Type__c,Description__c from concierge__c  where RecordTypeId = :UtilityClass.Concierge_Ticket ];
	}
	return concierges;
	} set;} 
	
	
	
	public String actionList{get{
	
	return JSON.serializePretty([select Name from AutomateSolution__c]);
	} set;} 
	
	
	
	
	  public  string JsonConcierges{get{
	
	    if(JsonConcierges==null)
	    {
	     JsonConcierges = JSON.serializePretty(concierges);
	    }
	    return JsonConcierges;
	       } set;}         
	        
	//wrapperObject for Concierge 
	global  class Concierge{
	        
	        global string id {get; set;}
	        global string name{get; set;}
	        global string status {get; set;}
	        global string region{get; set;}
	        global Date   createdDate{get; set;}
	        global Date   closedDate{get; set;}
	        global string  OwnerName{get; set;}
	        global string RecordTypeName{get; set;}
	        global string Description{get; set;}
	        
	        global  Concierge(){
	                
	        }
	        
	        
	} 
	
	
	public void refreshFromServer(){
	       
	         
	         list<concierge__c> conciergetoDelete = [select id ,Ext_Id__c from concierge__c where RecordTypeId = :UtilityClass.Concierge_Ticket];
	         Set<String> conciergeSaved = new Set<String>();
	         for(concierge__c thisConcierge : conciergetoDelete){
	          conciergeSaved.add(thisConcierge.Ext_Id__c);
	         }
	         List<Concierge__c> conciergesToinsert = new list<Concierge__c>();
	        // Date searchDate = Date.newInstance(2015,11,24);
	         Set<String> conciergeId = new Set<String>();
	        // SavePoint sp1 = Database.setSavePoint();
	
	         //Database.commit(sp1);
	        
	        ConciergeReportForIH cr = new ConciergeReportForIH(); 
	        List<String> logginDetail = cr.getSessionID();
	        partnerSoapSforceCom.Soap sp= new partnerSoapSforceCom.Soap();
	        sp.endpoint_x= logginDetail[0];
	        partnerSoapSforceCom.SessionHeader_element header= new    partnerSoapSforceCom.SessionHeader_element();
	        header.sessionId=logginDetail[1];
	        sp.SessionHeader= header;
	        string myQuery = 'select id , name ,CreatedDate,Closed_Date__c,Summary__c ,Status__c,Region__c ,ownerId ,Request_Area__c, Request_Type__c,Description__c from Concierge__c where  RecordType.DeveloperName =\'SF_Request\' and Status__c =\'New\' ORDER BY CreatedDate DESC limit 100 ';
	         
	         partnerSoapSforceCom.QueryResult  ConciergeResult = sp.query(myQuery);
	        sobjectPartnerSoapSforceCom.sObject_x[] ConciergeRecord = ConciergeResult.records;
	         system.debug(ConciergeRecord);
	        
	        
	        if( ConciergeRecord !=null && !ConciergeRecord.isEmpty()){
	        
	            DateTime  todayDate  = DateTime.now().addDays(-1);
	            
	            String dayValue = todayDate.format('EEEE');
	            system.debug('day is '+ dayValue);
	            if(dayValue.equalsIgnoreCase('Sunday')){
	            todayDate = todayDate.addDays(-2);
	            }
	            else if(dayValue.equalsIgnoreCase('Saturday')){
	            
	             todayDate = todayDate.addDays(-1);
	            }
	            
	         
	         for(sobjectPartnerSoapSforceCom.sObject_x sob : ConciergeRecord){
	         
	               if(conciergeId.contains(sob.id))
	                  continue;
	                 
	                 
	                conciergeId.add(sob.id);
	              
	               
	                    
	                    Concierge__c crg = new Concierge__c();
	                    crg.Ext_Id__c = sob.id;
	                    crg.name = sob.name;
	                    crg.status__c = sob.status;
	                    crg.Closed_Date__c = sob.closedDate;
	                    crg.Created_Date__c= sob.CreatedDate.date();
	                    crg.RecordTypeId = UtilityClass.Concierge_Ticket;
	                    crg.Description__c = sob.Description != null ?sob.Description.replaceAll('<[^>]+>','\n') : sob.Description;           
	                    crg.OtherInformation__c = sob.OtherInformation != null ?sob.OtherInformation.replaceAll('<[^>]+>','\n') : sob.OtherInformation;
	                    crg.Region__c = sob.Region;
	                    crg.Request_Area__c = sob.RequestArea;
	                    crg.Request_Type__c = sob.RequestType;    
	                    crg.Summary__c = sob.Summary;   
	                    crg.Description_assigned__c = sob.Description != null ?sob.Description.replaceAll('<[^>]+>','\n') : sob.Description;     
	                    if(sob.OwnerName == '00GE0000001X62oMAC'|| sob.OwnerName =='005E0000006fjEeIAI'){
	                     crg.Owner_Name__c   = (sob.OwnerName == '00GE0000001X62oMAC')? 'SF Support' : 'Saurabh kumar';  
	                     conciergesToinsert .add(crg);    
	                    }        
	                   
	                 }
	         }
	            
	        delete conciergetoDelete ;         
	        if(conciergesToinsert .size()!=0)
	        {
	           insert conciergesToinsert ;
	           concierges.clear();
	           concierges.addAll(conciergesToinsert);
	           JsonConcierges = JSON.serializePretty(concierges);
	           
	         //Savepoint sp1 = Database.setSavepoint();
	        }
	 
	}   
	
	@RemoteAction
	 Public Static String AssignedIt(String TicketId ,String myId){
	 String  result = '';
	 sobjectPartnerSoapSforceCom.sObject_x sob = new sobjectPartnerSoapSforceCom.sObject_x();
	 sob.type_x ='Concierge__c';
	 sob.id = TicketId;
	 sob.OwnerName = Label.Saurabh_kumar;
	 
	 
	 ConciergeReportForIH cr = new ConciergeReportForIH(); 
	 List<String> logginDetail = cr.getSessionID();
	 partnerSoapSforceCom.Soap sp= new partnerSoapSforceCom.Soap();
	 sp.endpoint_x= logginDetail[0];
	 partnerSoapSforceCom.SessionHeader_element header= new    partnerSoapSforceCom.SessionHeader_element();
	 header.sessionId=logginDetail[1];
	 sp.SessionHeader= header;
	 
	 partnerSoapSforceCom.SaveResult[] svResult = sp.update_x( new List<sobjectPartnerSoapSforceCom.sObject_x>{sob} );
	 if(svResult != null && svResult[0].success){
	 result = 'Completed';
	 //Changing current Concierge Record Type to Assigened
	 Concierge__c crg = new Concierge__c(id =myId,RecordTypeId = UtilityClass.Concierge_Assigned);
	 update crg;
     //fetching Attachement to this ticket if any 
     insertAttachmentForRelatedConcierge(new Set<Id>{TicketId});  
	 }
	 else{
	 result = svResult[0].errors[0].message;
	 }
	 
	 return result;
	 }
	
	/*
	@RemoteAction
	 Public Static String Assignedto(String TicketId ){
	 String  result = '';
	 sobjectPartnerSoapSforceCom.sObject_x sob = new sobjectPartnerSoapSforceCom.sObject_x();
	 sob.type_x ='Concierge__c';
	 sob.id = TicketId;
	 sob.OwnerName = Label.Saurabh_kumar;
	 
	 
	 ConciergeReportForIH cr = new ConciergeReportForIH(); 
	 List<String> logginDetail = cr.getSessionID();
	 partnerSoapSforceCom.Soap sp= new partnerSoapSforceCom.Soap();
	 sp.endpoint_x= logginDetail[0];
	 partnerSoapSforceCom.SessionHeader_element header= new    partnerSoapSforceCom.SessionHeader_element();
	 header.sessionId=logginDetail[1];
	 sp.SessionHeader= header;
	 
	 partnerSoapSforceCom.SaveResult[] svResult = sp.update_x( new List<sobjectPartnerSoapSforceCom.sObject_x>{sob} );
	 if(svResult != null && svResult[0].success){
	 result = 'Completed';
	 //Changing current Concierge Record Type to Assigened
	 //Concierge__c crg = new Concierge__c(id =myId,RecordTypeId = UtilityClass.Concierge_Assigned);
	 //update crg;
	 }
	 else{
	 result = svResult[0].errors[0].message;
	 }
	 
	 return result;
	 }
	   */
	   
	 @Future(Callout = true)
	 public static void insertAttachmentForRelatedConcierge(Set<Id> conciergeExtId){
	 
	 	Map<ID,ID> mapExtIDwithID = new Map<ID ,ID>();
	 	List<Attachment> listAttachment = new List<Attachment>();
	 	for(Concierge__c concierge : [Select ID ,Ext_Id__c From Concierge__c where Ext_Id__c IN :conciergeExtID]){
	 		mapExtIDwithID.put(concierge.Ext_Id__c , concierge.ID);
	 	}
	 	//fetching all attachment associated with provided Concierge
	 	ConciergeReportForIH cr = new ConciergeReportForIH(); 
        List<String> logginDetail = cr.getSessionID();
        partnerSoapSforceCom.Soap sp= new partnerSoapSforceCom.Soap();
        sp.endpoint_x= logginDetail[0];
        partnerSoapSforceCom.SessionHeader_element header= new    partnerSoapSforceCom.SessionHeader_element();
        header.sessionId=logginDetail[1];
        sp.SessionHeader= header;
        string myQuery = 'Select id , name, Body, ContentType ,Parentid from Attachment where  Parentid IN (';
        for(Id con :conciergeExtId ){
        	myQuery = myQuery + '\''+ con+'\',';
        }
        System.debug('myQuery'+myQuery); 
        myQuery = myQuery.removeEnd(',');
        myQuery = myQuery +')';
         
        System.debug('myQuery'+myQuery); 
        partnerSoapSforceCom.QueryResult  AttachmentResults = sp.query(myQuery);
        sobjectPartnerSoapSforceCom.sObject_x[] AttachmentRecord = AttachmentResults.records;
        system.debug(AttachmentRecord);
        
        if(AttachmentRecord.isEmpty())
        return;
        
        for(sobjectPartnerSoapSforceCom.sObject_x sob : AttachmentRecord){
	         
		     Attachment  att = new Attachment ();
	         att.body = EncodingUtil.base64Decode(sob.Body);
	         att.name = sob.name;
	         att.ContentType = sob.ContentType;
	         att.Parentid = mapExtIDwithID.get(sob.Parentid);
	         listAttachment.add(att);    
	     }        
	    insert listAttachment;               
	                 
	    }
	 	
	 
      

}