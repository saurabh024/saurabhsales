//class for making report on concierge in IH org 
global  class ConciergeReportForIHWSR {
public List<Concierge__c> concierges{get{
if(concierges == null ){
concierges = [select Ext_Id__c ,Name,Status__c,Region__c,Summary__c ,Closed_Date__c,Created_Date__c,Owner_Name__c, Requestor_Name__c,Request_Area__c, Request_Type__c from concierge__c where RecordTypeId = :UtilityClass.Concierge_DSR];
}
return concierges;
} set;}  

  public  string JsonConcierges{get{

    if(JsonConcierges==null)
    {
     JsonConcierges = JSON.serializePretty(concierges);
    }
    return JsonConcierges;
       } set;}         
        
//wrapperObject for Concierge 
global  class Concierge{
        
        global string id {get; set;}
        global string name{get; set;}
        global string status {get; set;}
        global string region{get; set;}
        global Date   createdDate{get; set;}
        global Date   closedDate{get; set;}
        global string  OwnerName{get; set;}
        global string RecordTypeName{get; set;}
        
        global  Concierge(){
                
        }
        
        
}       
        
        
 // method for loggin into IH Sf org and retrieve session id 
 public  list<String> getSessionID(){
        
            partnerSoapSforceCom.LoginResult loginResult;       
        partnerSoapSforceCom.Soap sp= new partnerSoapSforceCom.Soap();
        List<String> loginInfo = new list<string>();

        String username = UtilityClass.Username;
        String password = UtilityClass.Password;
        loginResult = sp.login(username, password);
        system.debug('loginResult ' + loginResult);
        loginInfo.add(loginResult.serverUrl);
        loginInfo.add(loginResult.sessionId);
        return  loginInfo;
        
 }      
 
 
 public void retrieveConcierge()
 {       
         
         list<concierge__c> conciergetoDelete = [select id ,Ext_Id__c from concierge__c where RecordTypeId = :UtilityClass.Concierge_DSR];
         Set<String> conciergeSaved = new Set<String>();
         for(concierge__c thisConcierge : conciergetoDelete){
          conciergeSaved.add(thisConcierge.Ext_Id__c);
         }
         List<Concierge__c> conciergesToinsert = new list<Concierge__c>();
        // Date searchDate = Date.newInstance(2015,11,24);
         Set<String> conciergeId = new Set<String>();
        // SavePoint sp1 = Database.setSavePoint();

         //Database.commit(sp1);
         
        List<String> logginDetail = getSessionID();
        partnerSoapSforceCom.Soap sp= new partnerSoapSforceCom.Soap();
        sp.endpoint_x= logginDetail[0];
        partnerSoapSforceCom.SessionHeader_element header= new    partnerSoapSforceCom.SessionHeader_element();
        header.sessionId=logginDetail[1];
        sp.SessionHeader= header;
        string myQuery = 'select id , name ,CreatedDate,Closed_Date__c,Summary__c ,Status__c,Region__c ,ownerId ,Request_Area__c, Request_Type__c from Concierge__c where  RecordType.DeveloperName =\'SF_Request\' ORDER BY CreatedDate DESC limit 200 ';
        string myQuery1 = 'select id , name ,CreatedDate,Closed_Date__c,Summary__c,Status__c,Region__c ,ownerId ,Request_Area__c, Request_Type__c from Concierge__c where  RecordType.DeveloperName =\'SF_Request\'  AND (Request_Area__c = \'Support Issue\' OR  Request_Area__c = \'User Access\' ) And  Request_Type__c != \'DATA CHANGES\' And Status__c != \'Closed\' And Status__c != \'Completed\' ';
        string myQuery2 = 'select id , name ,CreatedDate,Closed_Date__c,Summary__c,Status__c,Region__c ,ownerId ,Request_Area__c, Request_Type__c from Concierge__c where  RecordType.DeveloperName =\'SF_Request\'  AND Closed_Date__c > LAST_WEEK';
        partnerSoapSforceCom.QueryResult  ConciergeResult = sp.query(myQuery);
        sobjectPartnerSoapSforceCom.sObject_x[] ConciergeRecord = ConciergeResult.records;
         system.debug(ConciergeRecord);
        partnerSoapSforceCom.QueryResult  ConciergeResult1 = sp.query(myQuery1 );
        sobjectPartnerSoapSforceCom.sObject_x[] ConciergeRecord1 = ConciergeResult1.records;
        system.debug(ConciergeRecord1);
         ConciergeRecord.addAll(ConciergeRecord1);
         
         partnerSoapSforceCom.QueryResult  ConciergeResult2 = sp.query(myQuery2 );
         sobjectPartnerSoapSforceCom.sObject_x[] ConciergeRecord2 = ConciergeResult2.records;
         system.debug(ConciergeRecord2);
         ConciergeRecord.addAll(ConciergeRecord2);
        
        
        if( ConciergeRecord !=null && !ConciergeRecord.isEmpty()){
         
         for(sobjectPartnerSoapSforceCom.sObject_x sob : ConciergeRecord){
         
               if(conciergeId.contains(sob.id))
                  continue;
                 
                 
                conciergeId.add(sob.id);
              
               
                    
                    Concierge__c crg = new Concierge__c();
                    crg.Ext_Id__c = sob.id;
                    crg.name = sob.name;
                    crg.status__c = sob.status;
                    crg.Closed_Date__c = sob.closedDate;
                    crg.Created_Date__c= sob.CreatedDate.date();
                    
                    
                   // system.debug('sob date' + sob.CreatedDate.addHours(-6) + 'ticket ' + sob.name);
                    //system.debug('date type' +date.today());
                    //(sob.createdDate== date.today()-2 && sob.closedDate == date.today()-1 )|| (crg.Owner_Name__c == 'IH Team' && sob.createdDate== date.today()-1 &&( sob.closedDate == date.today()-1 ||sob.closedDate == null))
                   if(crg.Created_Date__c >= date.today()-6  ||  sob.status !='Closed'|| crg.Closed_Date__c >= date.today()-4){   
                   
                   
                    if(sob.OwnerName == '005E0000007MD8GIAW')
                    crg.Owner_Name__c   =  'Ashish Varma';
                    else if(sob.OwnerName == '005E0000006fjEeIAI')
                    crg.Owner_Name__c = 'Saurabh Kumar';
                    else if (sob.OwnerName =='005E0000001qfIQIAY'||sob.OwnerName =='005E0000000JyO1IAK'||sob.OwnerName=='005E0000001sp7kIAA'||sob.OwnerName=='005E0000000K7OAIA0')
                    crg.Owner_Name__c = 'Bhavi\'s Team';
                    else if (sob.OwnerName =='005E0000003RcUUIA0')
                    crg.Owner_Name__c = 'Naveen Skaria';
                    else{
                     crg.Owner_Name__c = 'IH Team';
                    }
                    //crg.RecordTypeName= sob.ConciergeType;
                    crg.Region__c = sob.Region;
                    crg.Request_Area__c = sob.RequestArea;
                    crg.Request_Type__c = sob.RequestType;    
                    crg.Summary__c = sob.Summary;  
                    crg.RecordTypeId = UtilityClass.Concierge_DSR;               
                    conciergesToinsert .add(crg);
                 }
         }
                
        }
        if(conciergesToinsert .size()!=0)
        {
           insert conciergesToinsert ;
           concierges.clear();
           concierges.addAll(conciergesToinsert);
           JsonConcierges = JSON.serializePretty(concierges);
           delete conciergetoDelete ;
         //Savepoint sp1 = Database.setSavepoint();
        }
 } 

}