/**
Develop by :- Saurabh kumar 
Date : 9/22/2016
**/
public with sharing class ExecuteAnonymousCode {


//Method to login into Salesforce 

 public  list<String> getSessionID(){
        
        toolingSoapSforceCom.LoginResult loginResult;       
        toolingSoapSforceCom.SforceService ss= new toolingSoapSforceCom.SforceService();
        List<String> loginInfo = new list<string>();

        String username = UtilityClass.Username;
        String password = UtilityClass.Password;
        loginResult = ss.login(username, password);
        system.debug('loginResult ' + loginResult);
        loginInfo.add(loginResult.serverUrl);
        loginInfo.add(loginResult.sessionId);
        return  loginInfo;
        
 } 
 
 
 //execute anonymous block 
 
 public String executeCodeonSF(String anonymous){
 	List<String> logginDetail = getSessionID();
 	toolingSoapSforceCom.ExecuteAnonymousResult exAnonymousResult;
 	toolingSoapSforceCom.SforceService ss= new toolingSoapSforceCom.SforceService();
 	ss.endpoint_x = logginDetail[0];
 	
 	//creating session header and adding to existing sforceservices 
 	toolingSoapSforceCom.SessionHeader_element sHeader = new toolingSoapSforceCom.SessionHeader_element();
 	sHeader.sessionId = logginDetail[1];
 	ss.SessionHeader = sHeader;
 	
 	exAnonymousResult = ss.executeAnonymous(anonymous);
 	system.debug('exAnonymousResult' +exAnonymousResult);
 	return 'success';
 	}    
    
     Public String executeCodewithLoginDetail(String anonymous ,List<String> logginDetail){
 	
 	toolingSoapSforceCom.ExecuteAnonymousResult exAnonymousResult;
 	toolingSoapSforceCom.SforceService ss= new toolingSoapSforceCom.SforceService();
 	ss.endpoint_x = logginDetail[0];
 	
 	//creating session header and adding to existing sforceservices 
 	toolingSoapSforceCom.SessionHeader_element sHeader = new toolingSoapSforceCom.SessionHeader_element();
 	sHeader.sessionId = logginDetail[1];
 	ss.SessionHeader = sHeader;
 	
 	exAnonymousResult = ss.executeAnonymous(anonymous);
 	system.debug('exAnonymousResult' +exAnonymousResult);
 	return 'success';
 	}    

}