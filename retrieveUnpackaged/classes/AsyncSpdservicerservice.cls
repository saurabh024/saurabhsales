//Generated by wsdl2apex

public class AsyncSpdservicerservice {
    public class CallNotesResponceFuture extends System.WebServiceCalloutFuture {
        public spdservicerservice.CallNotesResponce getValue() {
            spdservicerservice.CallNotesResponce response = (spdservicerservice.CallNotesResponce)System.WebServiceCallout.endInvoke(this);
            return response;
        }
    }
    public class ResponseInfoFuture extends System.WebServiceCalloutFuture {
        public spdservicerservice.ResponseInfo getValue() {
            spdservicerservice.ResponseInfo response = (spdservicerservice.ResponseInfo)System.WebServiceCallout.endInvoke(this);
            return response;
        }
    }
    public class ProductCoverageResponseFuture extends System.WebServiceCalloutFuture {
        public spdservicerservice.ProductCoverageResponse getValue() {
            spdservicerservice.ProductCoverageResponse response = (spdservicerservice.ProductCoverageResponse)System.WebServiceCallout.endInvoke(this);
            return response;
        }
    }
    public class CallInfoResponceFuture extends System.WebServiceCalloutFuture {
        public spdservicerservice.CallInfoResponce getValue() {
            spdservicerservice.CallInfoResponce response = (spdservicerservice.CallInfoResponce)System.WebServiceCallout.endInvoke(this);
            return response;
        }
    }
    public class CallAttributesResponseFuture extends System.WebServiceCalloutFuture {
        public spdservicerservice.CallAttributesResponse getValue() {
            spdservicerservice.CallAttributesResponse response = (spdservicerservice.CallAttributesResponse)System.WebServiceCallout.endInvoke(this);
            return response;
        }
    }
    public class TestServiceResponseFuture extends System.WebServiceCalloutFuture {
        public String getValue() {
            spdservicerservice.TestServiceResponse response = (spdservicerservice.TestServiceResponse)System.WebServiceCallout.endInvoke(this);
            return response.TestServiceResponse;
        }
    }
    public class AsyncSPDService {
        public String endpoint_x = 'http://fssstag.servicepower.com/sms/services/SPDService';
        public Map<String,String> inputHttpHeaders_x;
        public String clientCertName_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'urn:SPDServicerService', 'spdservicerservice'};
        public AsyncSpdservicerservice.CallNotesResponceFuture beginGetCallNotes(System.Continuation continuation,spdservicerservice.UserInfo UserInfo,String FromDateTime,String ToDateTime,String Callno) {
            spdservicerservice.CallInfoSearch request_x = new spdservicerservice.CallInfoSearch();
            request_x.UserInfo = UserInfo;
            request_x.FromDateTime = FromDateTime;
            request_x.ToDateTime = ToDateTime;
            request_x.Callno = Callno;
            return (AsyncSpdservicerservice.CallNotesResponceFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncSpdservicerservice.CallNotesResponceFuture.class,
              continuation,
              new String[]{endpoint_x,
              '',
              'urn:SPDServicerService',
              'getCallNotesRequest',
              'urn:SPDServicerService',
              'getCallNotesResponse',
              'spdservicerservice.CallNotesResponce'}
            );
        }
        public AsyncSpdservicerservice.ResponseInfoFuture beginUpdateTechCapacity(System.Continuation continuation,spdservicerservice.UserInfo UserInfo,spdservicerservice.TechCapacityByDate[] techCapacity) {
            spdservicerservice.MultTechCapacity request_x = new spdservicerservice.MultTechCapacity();
            request_x.UserInfo = UserInfo;
            request_x.techCapacity = techCapacity;
            return (AsyncSpdservicerservice.ResponseInfoFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncSpdservicerservice.ResponseInfoFuture.class,
              continuation,
              new String[]{endpoint_x,
              '',
              'urn:SPDServicerService',
              'updateTechCapcity',
              'urn:SPDServicerService',
              'getResponseInfo2',
              'spdservicerservice.ResponseInfo'}
            );
        }
        public AsyncSpdservicerservice.ResponseInfoFuture beginUpdateAreaInfo(System.Continuation continuation,spdservicerservice.UserInfo UserInfo,String AreaKey,String AreaName,String DispatchOfficeName,spdservicerservice.ArrayOf_xsd_string GroupKey,spdservicerservice.ArrayOf_xsd_string PostcodeList) {
            spdservicerservice.AreaInfo request_x = new spdservicerservice.AreaInfo();
            request_x.UserInfo = UserInfo;
            request_x.AreaKey = AreaKey;
            request_x.AreaName = AreaName;
            request_x.DispatchOfficeName = DispatchOfficeName;
            request_x.GroupKey = GroupKey;
            request_x.PostcodeList = PostcodeList;
            return (AsyncSpdservicerservice.ResponseInfoFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncSpdservicerservice.ResponseInfoFuture.class,
              continuation,
              new String[]{endpoint_x,
              '',
              'urn:SPDServicerService',
              'getAreaInfo1',
              'urn:SPDServicerService',
              'getResponseInfo6',
              'spdservicerservice.ResponseInfo'}
            );
        }
        public AsyncSpdservicerservice.ProductCoverageResponseFuture beginGetProductCoverage(System.Continuation continuation,spdservicerservice.UserInfo UserInfo,String FSSCallId) {
            spdservicerservice.ProductCoverageInfo request_x = new spdservicerservice.ProductCoverageInfo();
            request_x.UserInfo = UserInfo;
            request_x.FSSCallId = FSSCallId;
            return (AsyncSpdservicerservice.ProductCoverageResponseFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncSpdservicerservice.ProductCoverageResponseFuture.class,
              continuation,
              new String[]{endpoint_x,
              '',
              'urn:SPDServicerService',
              'getProductCoverageRequest',
              'urn:SPDServicerService',
              'getProductCoverageResponse',
              'spdservicerservice.ProductCoverageResponse'}
            );
        }
        public AsyncSpdservicerservice.ResponseInfoFuture beginUpdateTechInfo(System.Continuation continuation,spdservicerservice.UserInfo UserInfo,String Name,String Key,String GroupKey,String CellPhone,spdservicerservice.TechCapacityByDay[] BasicCapacity) {
            spdservicerservice.TechInfo request_x = new spdservicerservice.TechInfo();
            request_x.UserInfo = UserInfo;
            request_x.Name = Name;
            request_x.Key = Key;
            request_x.GroupKey = GroupKey;
            request_x.CellPhone = CellPhone;
            request_x.BasicCapacity = BasicCapacity;
            return (AsyncSpdservicerservice.ResponseInfoFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncSpdservicerservice.ResponseInfoFuture.class,
              continuation,
              new String[]{endpoint_x,
              '',
              'urn:SPDServicerService',
              'getTechInfo1',
              'urn:SPDServicerService',
              'getResponseInfo1',
              'spdservicerservice.ResponseInfo'}
            );
        }
        public AsyncSpdservicerservice.CallInfoResponceFuture beginGetCallInfo(System.Continuation continuation,spdservicerservice.UserInfo UserInfo,String FromDateTime,String ToDateTime,String Callno) {
            spdservicerservice.CallInfoSearch request_x = new spdservicerservice.CallInfoSearch();
            request_x.UserInfo = UserInfo;
            request_x.FromDateTime = FromDateTime;
            request_x.ToDateTime = ToDateTime;
            request_x.Callno = Callno;
            return (AsyncSpdservicerservice.CallInfoResponceFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncSpdservicerservice.CallInfoResponceFuture.class,
              continuation,
              new String[]{endpoint_x,
              '',
              'urn:SPDServicerService',
              'getCallInfoSearch',
              'urn:SPDServicerService',
              'getCallInfoResponce',
              'spdservicerservice.CallInfoResponce'}
            );
        }
        public AsyncSpdservicerservice.ResponseInfoFuture beginAddAreaInfo(System.Continuation continuation,spdservicerservice.UserInfo UserInfo,String AreaKey,String AreaName,String DispatchOfficeName,spdservicerservice.ArrayOf_xsd_string GroupKey,spdservicerservice.ArrayOf_xsd_string PostcodeList) {
            spdservicerservice.AreaInfo request_x = new spdservicerservice.AreaInfo();
            request_x.UserInfo = UserInfo;
            request_x.AreaKey = AreaKey;
            request_x.AreaName = AreaName;
            request_x.DispatchOfficeName = DispatchOfficeName;
            request_x.GroupKey = GroupKey;
            request_x.PostcodeList = PostcodeList;
            return (AsyncSpdservicerservice.ResponseInfoFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncSpdservicerservice.ResponseInfoFuture.class,
              continuation,
              new String[]{endpoint_x,
              '',
              'urn:SPDServicerService',
              'getAreaInfo',
              'urn:SPDServicerService',
              'getResponseInfo5',
              'spdservicerservice.ResponseInfo'}
            );
        }
        public AsyncSpdservicerservice.CallAttributesResponseFuture beginGetCallAttributes(System.Continuation continuation,spdservicerservice.UserInfo UserInfo,String FSSCallId) {
            spdservicerservice.CallAttributesInfo request_x = new spdservicerservice.CallAttributesInfo();
            request_x.UserInfo = UserInfo;
            request_x.FSSCallId = FSSCallId;
            return (AsyncSpdservicerservice.CallAttributesResponseFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncSpdservicerservice.CallAttributesResponseFuture.class,
              continuation,
              new String[]{endpoint_x,
              '',
              'urn:SPDServicerService',
              'getCallAttributesRequest',
              'urn:SPDServicerService',
              'getCallAttributesResponse',
              'spdservicerservice.CallAttributesResponse'}
            );
        }
        public AsyncSpdservicerservice.TestServiceResponseFuture beginGetTestService(System.Continuation continuation,String TestService) {
            spdservicerservice.TestService request_x = new spdservicerservice.TestService();
            request_x.TestService = TestService;
            return (AsyncSpdservicerservice.TestServiceResponseFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncSpdservicerservice.TestServiceResponseFuture.class,
              continuation,
              new String[]{endpoint_x,
              '',
              'urn:SPDServicerService',
              'getTestService',
              'urn:SPDServicerService',
              'getTestServiceResponse',
              'spdservicerservice.TestServiceResponse'}
            );
        }
        public AsyncSpdservicerservice.ResponseInfoFuture beginUpdateCallInfo(System.Continuation continuation,spdservicerservice.UserInfo UserInfo,String CallNumber,String MfgId,String FSSCallId,String ScheduleDate,String ScheduleTimePeriod,String ProbelmDesc,String CallStatus,String SPCallStatusID,String CallSubStatus,String SPCallSubStatusID,spdservicerservice.ProductInfo ProductInfo,spdservicerservice.ConsumerInfo ConsumerInfo,spdservicerservice.PartsInfo Parts,spdservicerservice.Remarks Remarks,String ETA,String ETF,String TechName,spdservicerservice.productReplacementInfo ReplaceProduct) {
            spdservicerservice.UpdateCall request_x = new spdservicerservice.UpdateCall();
            request_x.UserInfo = UserInfo;
            request_x.CallNumber = CallNumber;
            request_x.MfgId = MfgId;
            request_x.FSSCallId = FSSCallId;
            request_x.ScheduleDate = ScheduleDate;
            request_x.ScheduleTimePeriod = ScheduleTimePeriod;
            request_x.ProbelmDesc = ProbelmDesc;
            request_x.CallStatus = CallStatus;
            request_x.SPCallStatusID = SPCallStatusID;
            request_x.CallSubStatus = CallSubStatus;
            request_x.SPCallSubStatusID = SPCallSubStatusID;
            request_x.ProductInfo = ProductInfo;
            request_x.ConsumerInfo = ConsumerInfo;
            request_x.Parts = Parts;
            request_x.Remarks = Remarks;
            request_x.ETA = ETA;
            request_x.ETF = ETF;
            request_x.TechName = TechName;
            request_x.ReplaceProduct = ReplaceProduct;
            return (AsyncSpdservicerservice.ResponseInfoFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncSpdservicerservice.ResponseInfoFuture.class,
              continuation,
              new String[]{endpoint_x,
              '',
              'urn:SPDServicerService',
              'updateCallInfoObj',
              'urn:SPDServicerService',
              'getResponseInfo9',
              'spdservicerservice.ResponseInfo'}
            );
        }
        public AsyncSpdservicerservice.ResponseInfoFuture beginUpdateDispatchOfficeInfo(System.Continuation continuation,spdservicerservice.UserInfo UserInfo,String OfficeName,String ContactName,String Address1,String Address2,String PostcodeLevel1,String PostcodeLevel2,String PostcodeLevel3,String Postcode,String Country,String Email,String Fax,String IVRPhoneNumber,String EmailNotification,String FaxNotification,String IVRNotification) {
            spdservicerservice.DispatchInfo request_x = new spdservicerservice.DispatchInfo();
            request_x.UserInfo = UserInfo;
            request_x.OfficeName = OfficeName;
            request_x.ContactName = ContactName;
            request_x.Address1 = Address1;
            request_x.Address2 = Address2;
            request_x.PostcodeLevel1 = PostcodeLevel1;
            request_x.PostcodeLevel2 = PostcodeLevel2;
            request_x.PostcodeLevel3 = PostcodeLevel3;
            request_x.Postcode = Postcode;
            request_x.Country = Country;
            request_x.Email = Email;
            request_x.Fax = Fax;
            request_x.IVRPhoneNumber = IVRPhoneNumber;
            request_x.EmailNotification = EmailNotification;
            request_x.FaxNotification = FaxNotification;
            request_x.IVRNotification = IVRNotification;
            return (AsyncSpdservicerservice.ResponseInfoFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncSpdservicerservice.ResponseInfoFuture.class,
              continuation,
              new String[]{endpoint_x,
              '',
              'urn:SPDServicerService',
              'getDispatchInfo',
              'urn:SPDServicerService',
              'getResponseInfo8',
              'spdservicerservice.ResponseInfo'}
            );
        }
        public AsyncSpdservicerservice.ResponseInfoFuture beginUpdateGroupInfo(System.Continuation continuation,spdservicerservice.UserInfo UserInfo,String Name,String Key,String DisPatchOfficeName,spdservicerservice.ArrayOf_xsd_string WarrantyType,spdservicerservice.ArrayOf_xsd_string ServiceLocation,spdservicerservice.ArrayOf_xsd_string ServiceType,spdservicerservice.ArrayOf_xsd_string Industry) {
            spdservicerservice.GroupInfo request_x = new spdservicerservice.GroupInfo();
            request_x.UserInfo = UserInfo;
            request_x.Name = Name;
            request_x.Key = Key;
            request_x.DisPatchOfficeName = DisPatchOfficeName;
            request_x.WarrantyType = WarrantyType;
            request_x.ServiceLocation = ServiceLocation;
            request_x.ServiceType = ServiceType;
            request_x.Industry = Industry;
            return (AsyncSpdservicerservice.ResponseInfoFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncSpdservicerservice.ResponseInfoFuture.class,
              continuation,
              new String[]{endpoint_x,
              '',
              'urn:SPDServicerService',
              'getGroupInfo1',
              'urn:SPDServicerService',
              'getResponseInfo4',
              'spdservicerservice.ResponseInfo'}
            );
        }
        public AsyncSpdservicerservice.ResponseInfoFuture beginUpdatePostcode(System.Continuation continuation,spdservicerservice.UserInfo UserInfo,spdservicerservice.PostcodeUpdate PostcodeUpdateObj) {
            spdservicerservice.updatePostcodeInfo request_x = new spdservicerservice.updatePostcodeInfo();
            request_x.UserInfo = UserInfo;
            request_x.PostcodeUpdateObj = PostcodeUpdateObj;
            return (AsyncSpdservicerservice.ResponseInfoFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncSpdservicerservice.ResponseInfoFuture.class,
              continuation,
              new String[]{endpoint_x,
              '',
              'urn:SPDServicerService',
              'updatePostcodeRequest',
              'urn:SPDServicerService',
              'updatePostcodeResponse',
              'spdservicerservice.ResponseInfo'}
            );
        }
        public AsyncSpdservicerservice.ResponseInfoFuture beginAddTechInfo(System.Continuation continuation,spdservicerservice.UserInfo UserInfo,String Name,String Key,String GroupKey,String CellPhone,spdservicerservice.TechCapacityByDay[] BasicCapacity) {
            spdservicerservice.TechInfo request_x = new spdservicerservice.TechInfo();
            request_x.UserInfo = UserInfo;
            request_x.Name = Name;
            request_x.Key = Key;
            request_x.GroupKey = GroupKey;
            request_x.CellPhone = CellPhone;
            request_x.BasicCapacity = BasicCapacity;
            return (AsyncSpdservicerservice.ResponseInfoFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncSpdservicerservice.ResponseInfoFuture.class,
              continuation,
              new String[]{endpoint_x,
              '',
              'urn:SPDServicerService',
              'getTechInfo',
              'urn:SPDServicerService',
              'getResponseInfo',
              'spdservicerservice.ResponseInfo'}
            );
        }
        public AsyncSpdservicerservice.ResponseInfoFuture beginAddGroupInfo(System.Continuation continuation,spdservicerservice.UserInfo UserInfo,String Name,String Key,String DisPatchOfficeName,spdservicerservice.ArrayOf_xsd_string WarrantyType,spdservicerservice.ArrayOf_xsd_string ServiceLocation,spdservicerservice.ArrayOf_xsd_string ServiceType,spdservicerservice.ArrayOf_xsd_string Industry) {
            spdservicerservice.GroupInfo request_x = new spdservicerservice.GroupInfo();
            request_x.UserInfo = UserInfo;
            request_x.Name = Name;
            request_x.Key = Key;
            request_x.DisPatchOfficeName = DisPatchOfficeName;
            request_x.WarrantyType = WarrantyType;
            request_x.ServiceLocation = ServiceLocation;
            request_x.ServiceType = ServiceType;
            request_x.Industry = Industry;
            return (AsyncSpdservicerservice.ResponseInfoFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncSpdservicerservice.ResponseInfoFuture.class,
              continuation,
              new String[]{endpoint_x,
              '',
              'urn:SPDServicerService',
              'getGroupInfo',
              'urn:SPDServicerService',
              'getResponseInfo3',
              'spdservicerservice.ResponseInfo'}
            );
        }
        public AsyncSpdservicerservice.ResponseInfoFuture beginUpdateServicerInfo(System.Continuation continuation,spdservicerservice.UserInfo UserInfo,String Name,String Address1,String Address2,String PostcodeLevel1,String PostcodeLevel2,String PostcodeLevel3,String Postcode,String Country,String Phone,String CellPhone,String FaxNo,String EmailID,spdservicerservice.ArrayOf_xsd_string TimeBand,spdservicerservice.ArrayOf_xsd_string Product,spdservicerservice.ArrayOf_xsd_string Brand) {
            spdservicerservice.ServicerInfo request_x = new spdservicerservice.ServicerInfo();
            request_x.UserInfo = UserInfo;
            request_x.Name = Name;
            request_x.Address1 = Address1;
            request_x.Address2 = Address2;
            request_x.PostcodeLevel1 = PostcodeLevel1;
            request_x.PostcodeLevel2 = PostcodeLevel2;
            request_x.PostcodeLevel3 = PostcodeLevel3;
            request_x.Postcode = Postcode;
            request_x.Country = Country;
            request_x.Phone = Phone;
            request_x.CellPhone = CellPhone;
            request_x.FaxNo = FaxNo;
            request_x.EmailID = EmailID;
            request_x.TimeBand = TimeBand;
            request_x.Product = Product;
            request_x.Brand = Brand;
            return (AsyncSpdservicerservice.ResponseInfoFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncSpdservicerservice.ResponseInfoFuture.class,
              continuation,
              new String[]{endpoint_x,
              '',
              'urn:SPDServicerService',
              'getServicerInfo',
              'urn:SPDServicerService',
              'getResponseInfo7',
              'spdservicerservice.ResponseInfo'}
            );
        }
    }
}