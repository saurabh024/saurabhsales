public with sharing  class ViewOpportunityWithSharing {
    
    public Static List<Opportunity> getOpp(){
        return [Select Id,Name from Opportunity limit 10];
    }
    
    public without sharing class InnerClass{
        
        public list<Opportunity> getOppr(){
             
            return [Select Id,Name from Opportunity limit 10];
        }
    }

}