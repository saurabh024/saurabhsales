//class for making report on concierge in IH org 
global  class LeaseManagementClass {
public List<My_Lead__c> allLeases{get{
if(allLeases== null ){
allLeases= [select Lead_Id__c,Name,LeaseRent__c,Housing_Rent__c,MTM__c ,MTM_Amount__c,Schedule_Date__c,Lease_Start_date__c , Lease_End_Date__c from My_Lead__c ];
}
return allLeases;
} set;} 

  public  string JsonallLeases{get{

    if(JsonallLeases==null)
    {
     JsonallLeases = JSON.serializePretty(allLeases);
    }
    return JsonallLeases;
       } set;}         
        
//wrapperObject for Concierge 
global  class Concierge{
        
        global string id {get; set;}
        global string name{get; set;}
        global string status {get; set;}
        global string region{get; set;}
        global Date   createdDate{get; set;}
        global Date   closedDate{get; set;}
        global string  OwnerName{get; set;}
        global string RecordTypeName{get; set;}
        
        global  Concierge(){
                
        }
        
        
}       
         
        
 // method for loggin into IH Sf org and retrieve session id 
 public static  list<String> getSessionID(){
        
        partnerSoapSforceCom.LoginResult loginResult;       
        partnerSoapSforceCom.Soap sp= new partnerSoapSforceCom.Soap();
        List<String> loginInfo = new list<string>();

        String username = UtilityClass.Username;
        String password = UtilityClass.Password + UtilityClass.securityToken;
        //String username = 'skumar@invitationhomes.com.uat';
        //String password = 'Cybage4invitation';
        loginResult = sp.login(username, password);
        system.debug('loginResult ' + loginResult);
        loginInfo.add(loginResult.serverUrl);
        loginInfo.add(loginResult.sessionId);
        return  loginInfo;
        
 }      
 
 
 //Method to Update Lease on Other System 
 @future(callout = true)
 public static void updateLeaseOnIH(){
           //Sobject Need to Be Updated in Other System 
           Set<String> MTMOptions = new Set<String>{'Yes' ,'No'};
           Set<Id> setExternalId = new Set<Id>();
     	   List<My_Lead__c> leaseInsideToUpdate = new List<My_Lead__c>();
           List<My_Lead__c> leasesFromInside = [select Lead_Id__c,Name,LeaseRent__c,Lease_Name__c,Housing_Rent__c,MTM__c ,MTM_Amount__c,Schedule_Date__c ,Lease_Start_date__c ,Lease_End_Date__c from My_Lead__c Where Schedule_Date__c >= Today];
           if(leasesFromInside == null || leasesFromInside.isEmpty())
               return;
           for(My_Lead__c ml : leasesFromInside){
                setExternalId.add(ml.lead_Id__c); 
           }
           
           //creating Soap Manager 
           //calling SOAP Services 
            List<String> logginDetail = getSessionID();
            String baseUrl = baseUrlfinder(logginDetail[0]);
            partnerSoapSforceCom.Soap sp= new partnerSoapSforceCom.Soap();
            sp.endpoint_x= logginDetail[0];
            partnerSoapSforceCom.SessionHeader_element header= new    partnerSoapSforceCom.SessionHeader_element();
            header.sessionId=logginDetail[1];
            sp.SessionHeader= header;
            //disabling feed tracking for this transaction 
            partnerSoapSforceCom.DisableFeedTrackingHeader_element  feedDisable = new partnerSoapSforceCom.DisableFeedTrackingHeader_element();
            feedDisable.disableFeedTracking = true;
            sp.DisableFeedTrackingHeader = feedDisable;
           
           //fetching value from IH 
           Map<Id ,sobjectPartnerSoapSforceCom.sObject_x> mapIdwithSob = getLeaseInfoFromOutside(setExternalId,sp);
           if(mapIdWithSob == null)
               return ;
     
           sobjectPartnerSoapSforceCom.sObject_x[] IHLeaseNeedTobeUpdated = new List<sobjectPartnerSoapSforceCom.sObject_x>();
           
            For(My_Lead__c mylead : leasesFromInside){
              if(!mapIdWithSob.containsKey(mylead.Lead_Id__c)) 
                  continue;
              
              //adding lease information on local lease records
                if(mylead.Lease_Name__c != mapIdWithSob.get(mylead.Lead_Id__c).Name){
                      mylead.Lease_Name__c  = mapIdWithSob.get(mylead.Lead_Id__c).Name;
                      mylead.lead_url__c =    baseUrl+'/'+mylead.Lead_Id__c;
                      leaseInsideToUpdate.add(mylead);
                  }     
                    
              sobjectPartnerSoapSforceCom.sObject_x newSob = new sobjectPartnerSoapSforceCom.sObject_x();
              newSob.Id = mylead.Lead_Id__c;
              newSob.type_x = 'Tenant_Card__c';
              Boolean isMtm = mylead.MTM__c == 'Yes'? true: (mylead.MTM__c == 'No'?false : null);
                  
              if(mylead.Housing_Rent__c != null && mapIdWithSob.get(mylead.Lead_Id__c).Housing_Rent != mylead.Housing_Rent__c)
              newSob.Housing_Rent = mylead.Housing_Rent__c;
              
              if(mylead.LeaseRent__c != null && mapIdWithSob.get(mylead.Lead_Id__c).Lease_Rent != mylead.LeaseRent__c)
              newSob.Lease_Rent = mylead.LeaseRent__c;
              
              if(mylead.MTM_Amount__c != null && mapIdWithSob.get(mylead.Lead_Id__c).MTM_Fee_Amount_Yardi != mylead.MTM_Amount__c)
              newSob.MTM_Fee_Amount_Yardi = mylead.MTM_Amount__c;
              
              if(MTMOptions.contains(mylead.MTM__c) && mapIdWithSob.get(mylead.Lead_Id__c).Month_to_Month != isMtm )
              newSob.Month_to_Month = mylead.MTM__c == 'Yes'?true:false;
              
              if(mylead.Lease_Start_Date__c != null && mapIdWithSob.get(mylead.Lead_Id__c).Lease_From_Date != mylead.Lease_Start_Date__c)
              newSob.Lease_From_Date = mylead.Lease_Start_Date__c;
              
              if(mylead.Lease_End_Date__c != null && mapIdWithSob.get(mylead.Lead_Id__c).Lease_End_Date != mylead.Lease_End_Date__c)
              newSob.Lease_End_Date = mylead.Lease_End_Date__c;
              
              
              if(newSob.Housing_Rent!=null || newSob.Lease_Rent != null || newSob.MTM_Fee_Amount_Yardi != null || newSob.Month_to_Month != null ||  newSob.Lease_End_Date != null || newSob.Lease_From_Date != null )
              IHLeaseNeedTobeUpdated.add(newSob);
              
           } 
        
       
     
        if(IHLeaseNeedTobeUpdated.isEmpty())
        return;
        
        
        
        try{
        partnerSoapSforceCom.SaveResult[]  saveResults = sp.update_x(IHLeaseNeedTobeUpdated);
        }catch(Exception e){
         System.debug(e.getMessage());
        }
        
         if(!leaseInsideToUpdate.isEmpty())
 		  update leasesFromInside;
   } 
 
    public static Map<Id ,sobjectPartnerSoapSforceCom.sObject_x> getLeaseInfoFromOutside(Set<Id> setleaseId ,partnerSoapSforceCom.Soap sp){
        
        if(setleaseId == null ||setleaseId.isEmpty())
            return null;
        
        string myQuery = 'select ID ,Name,Lease_Rent__c,Housing_Rent__c,MTM_Fee_Amount_Yardi__c ,Month_to_Month__c,Lease_From_Date__c,Lease_End_Date__c from Tenant_Card__c Where ID In (';
            for(String leaseId : setleaseId){
                myQuery = myQuery+'\''+leaseId+'\',';
            }
        myQuery =  myQuery.removeEnd(',');
        myQuery = myQuery+')';
         
        partnerSoapSforceCom.QueryResult  leasefromOutside = sp.query(myQuery);
        sobjectPartnerSoapSforceCom.sObject_x[] leasesExternal = leasefromOutside.records;
        Map<Id ,sobjectPartnerSoapSforceCom.sObject_x> maptoReturn = new Map<Id ,sobjectPartnerSoapSforceCom.sObject_x>();
        for(sobjectPartnerSoapSforceCom.sObject_x sob :leasesExternal){
            maptoReturn.put(sob.Id , sob);
        }
        system.debug(maptoReturn);
        return maptoReturn;
    }
    
    webservice static string runSchedular(){
        
        String runSchedularResult = '';
        List<CronTrigger> leaseUpdateCron = [SELECT Id, CronJobDetail.Id, CronJobDetail.Name, CronJobDetail.JobType FROM CronTrigger Where CronJobDetail.Name like 'UpdateLease%'];
        If(leaseUpdateCron == null || leaseUpdateCron.isEmpty()){
            
            //ScheduleupdateLeaseOutSide mySchdule = new ScheduleupdateLeaseOutSide ();
            DateTime currentTime = DateTime.Now().addMinutes(2);
      
            String cronstring = currentTime.second()+' '+currentTime.minute()+' '+currentTime.hour()+' * * ?';
            //calling next Schedular after 3 hrs .
            String jobID = system.schedule('UpdateLease1', cronstring, new ScheduleupdateLeaseOutSide ());
            runSchedularResult = 'Lease update Schdule for '+currentTime +'   JobId:'+jobID;
            //At the interval of 30 minutes from 6 : 30 AM to 9:30 AM
            currentTime = DateTime.newInstance(currentTime.year(), currentTime.month(), currentTime.day(), 6,00, 00);
            for(Integer i =0 ; i<6 ; i++){
                  currentTime = currentTime.addMinutes(30);
                  cronstring = currentTime.second()+' '+currentTime.minute()+' '+currentTime.hour()+' * * ?';
                  jobID = system.schedule('UpdateLease'+currentTime, cronstring, new ScheduleupdateLeaseOutSide ());
                  runSchedularResult = runSchedularResult +'\n' + 'Lease update Schdule for '+currentTime +'   JobId:'+jobID;
                  
            }
             currentTime = currentTime.addMinutes(120);
             cronstring = currentTime.second()+' '+currentTime.minute()+' '+currentTime.hour()+' * * ?';
              //calling next Schedular after 3 hrs .
             jobID = system.schedule('ScheduleLeaseUpdateSchdular'+currentTime, cronstring, new ScheduleTheSchedularLease ());
      
            
        }
        else{
            runSchedularResult = 'Lease Update is already Schdule It is  ';
            for(CronTrigger cro: leaseUpdateCron){
              runSchedularResult = runSchedularResult + '\n' + cro.CronJobDetail.Name;   
            }
        }
         
        return runSchedularResult;
        
    }
    
    
    webservice static string currentCronDetail(){
         
        String runCronResult = '';
        List<CronTrigger> leaseUpdateCron = [SELECT Id, CronJobDetail.Id, CronJobDetail.Name, CronJobDetail.JobType ,EndTime, NextFireTime, PreviousFireTime, StartTime, State, TimesTriggered  FROM CronTrigger Where CronJobDetail.Name like 'UpdateLease%' Limit 1 ];
        If(leaseUpdateCron == null || leaseUpdateCron.isEmpty()){
            
            
            runCronResult = 'Not Scheduled , Please click on runSchedular for Scheduling  ';
        }
        else{
            Long currentTime = System.now().getTime();
            Long scheduleTime = leaseUpdateCron[0].NextFireTime.getTime();
            Long milliseconds =  (scheduleTime - currentTime)/1000;
            Long Seconds = milliseconds/60;
            Long lefthours = milliseconds/(60*60);
            Long leftminutes = Math.mod(seconds ,60);
            Long leftseconds =  Math.mod(milliseconds ,60);
            
            runCronResult = 'LeaseUpdate schedular ' +leaseUpdateCron[0].CronJobDetail.Name +' is scheduled after  '+lefthours + ' hours '+ leftminutes +' minutes and '+leftseconds +'seconds';    
        }
         
        return runCronResult;
        
     }
    
    //function to fetch base url " ex https://login.salesforce.com"
    public static string baseUrlfinder(String wholeUrl){
            System.debug('***Url **'+wholeUrl);
            Pattern pt = Pattern.compile('(([a-z]{3,9})(:)(//)([a-z0-9])+(.)(?:[a-z]{0,12})+(.)([a-z]{0,3}))');
            //String myvar1 = '(?:(?:(?:[a-z0-9]{3,9}:(?://)?)(?:[-;:&=+$,w]+@)?[a-z0-9.-]+|(?:www.|[-;:&=+$??,w]+@)[a-z0-9.-]+)?)';
            //Pattern pt = Pattern.compile(myvar1);
            Matcher matcher = pt.matcher(wholeUrl);
            String surl = '';
            while(matcher.find()){
             surl = matcher.group();
             System.debug(surl);
             If(surl != '' && surl != null)
             break;
             
             } 
            return surl;
       }
    }