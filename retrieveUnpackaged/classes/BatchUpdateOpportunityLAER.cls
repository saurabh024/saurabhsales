public class BatchUpdateOpportunityLAER  implements Database.Batchable<String> , Database.AllowsCallouts,Database.stateful  {
   
    public  integer noofIteration;
    public List<String> logginDetail; 
    public BatchUpdateOpportunityLAER( ){
       logginDetail = new ExecuteAnonymousCode().getSessionID();
    }
    public Iterable<String> start(Database.BatchableContext BC) {
        
       list<String> listString = new List<String>();
       Integer noofiter = 9000;
        For(Integer i = 0; i< noofiter ; i++){
            listString.add(''+i);
        }
        return liststring;
    }

    public void execute(Database.BatchableContext info, List<String> strings) {
       
 
     AutomateSolution__c ac = [Select Id ,Anonymous_Code__c from AutomateSolution__c where Name ='LAERUPDATE'];   
        
  
                    
        String stringtoExecute =  ac.Anonymous_Code__c;
                   
        System.debug('*** Exceuted lines ***\n'+stringtoExecute);

         
          ExecuteAnonymousCode cd = new ExecuteAnonymousCode();          
        
                    
          System.debug(ac.Anonymous_Code__c);
          cd.executeCodewithLoginDetail(stringtoExecute ,logginDetail);
                  //  cd.executeCodeonSF(stringtoExecute);
        

    }

    public void finish(Database.BatchableContext info) {}
}