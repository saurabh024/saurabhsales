<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <tabs>standard-Chatter</tabs>
    <tabs>standard-Account</tabs>
    <tabs>standard-Contact</tabs>
    <tabs>standard-Case</tabs>
    <tabs>standard-Solution</tabs>
    <tabs>standard-report</tabs>
    <tabs>standard-Dashboard</tabs>
    <tabs>Concierge__c</tabs>
    <tabs>DSRANDWSR</tabs>
    <tabs>Employee__c</tabs>
    <tabs>LoggedInINFO__c</tabs>
    <tabs>My_Lead__c</tabs>
    <tabs>SIR__c</tabs>
    <tabs>AnonymousBlockCOde__c</tabs>
</CustomApplication>
